Giereczka-główny folder z grą
ExperimentalField-folder z projektem do testów - nie liczy się do końcowej wersji gry.
Gra pisana na Unity pod PC/Android. Wykorzystuje wheel colliders.
Gra polega na pokonywaniu przeciwników poprzez taranowanie im drogi. 
Za pojazdami ciągnie się ogon (trail) by pomóc w tym zadaniu.
Na mapie są trzy rodzaje powerupów.
Projekt wciąż rozwijany.

Twórcy:

Andrzej Szablewski @andrzejszablewski13 - główny programista(AI,movement)

Marta Maj @f_lox - programista(UI,bostery)

Paweł Macyszyn @nokiopaul - grafik 3D

Piotr Szymkiewicz @p45c41 - muzyk, gamedesigner

Nicola Samela - grafik 2D