﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Move : MonoBehaviour
{
    // Start is called before the first frame update
    private Rigidbody _rB;
    private float _inHor=0, _inVer=0;
    private Vector3 _direction;
    [SerializeField] private float _speed=5f;
    [SerializeField] private Joystick _joystick;
    private enum Moves
    {
        Horizontal,Vertical
    }
    void Start()
    {
        _rB = this.gameObject.GetComponent<Rigidbody>();
        if(SystemInfo.deviceType == DeviceType.Desktop && _joystick!=null)
        {
            _joystick.enabled = false;
        }
    }

    // Update is called once per frame
    private void Update()
    {
        if (SystemInfo.deviceType == DeviceType.Desktop)
        {
            _inHor = Input.GetAxis(Moves.Horizontal.ToString());
            _inVer = Input.GetAxis(Moves.Vertical.ToString());
        }
        else
        {
            _inHor = _joystick.Direction.x;
            _inVer = _joystick.Direction.y;
        }
    }
    void FixedUpdate()
    {
        
        _direction = new Vector3(_inHor, 0, _inVer);
        _rB.AddForce(_direction * Time.fixedDeltaTime * _speed, ForceMode.Impulse);
    }
}
