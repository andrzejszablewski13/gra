﻿using UnityEngine;
using UnityEngine.AI;
public class Idle2 : State
{
    public Idle2(GameObject _us, GameObject _target, NavMeshAgent _navMeshAgent)
                : base(_us, _target, _navMeshAgent)
    {
        name = STATE.IDLE; // Set name of current state.
    }

    public override void Enter()
    {
        base.Enter(); // Sets stage to UPDATE.
    }
    public override void Update()
    {
        nextState = new Patroling(us, target,navMeschAgent);
        stage = EVENT.EXIT;
    }

    public override void Exit()
    {
        base.Exit();
    }
}
