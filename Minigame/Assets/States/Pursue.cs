﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class Pursue : State
{
    
    private float _time = 0f;
    private float _neededTime = 2f;
    public Pursue(GameObject _us, GameObject _target,NavMeshAgent _navMeshAgent)
                : base(_us, _target, _navMeshAgent)
    {
        name = STATE.PURSUE; // Set name of current state.
    }

    public override void Enter()
    {
        base.Enter(); // Sets stage to UPDATE.
        navMeschAgent.destination = target.transform.position;
    }
    public override void Update()
    {
        if ((target.transform.position - us.transform.position).magnitude < _distance)
        {

            _time += Time.deltaTime;
            if (_time >= _neededTime)
            {
                nextState = new Attack(us, target,navMeschAgent);
                stage = EVENT.EXIT;
            }
        }
        else { _time = 0; 
            nextState = new Patroling(us, target, navMeschAgent);
            stage = EVENT.EXIT;
        }
        
    }

    public override void Exit()
    {
        base.Exit();
    }
}
