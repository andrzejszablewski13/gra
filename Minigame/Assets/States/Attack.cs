﻿using System.Collections;
using UnityEngine;
using UnityEngine.AI;
public class Attack : State
{
    private string _triggerAttack = "GotPlayer";
    private string _esceped = "Escape";
    private float _timebetwenShoot = 1f,_timeofVisibilityofShoot=0.1f,_time;
    private Animator _anim;
    private LineRenderer _lineShoot;
    private bool _visible = false,_pauze=false;
    private int _dmg = 5;
    public Attack(GameObject _us, GameObject _target, NavMeshAgent _navMeshAgent)
                : base(_us, _target,_navMeshAgent)
    {
        name = STATE.ATTACK; // Set name of current state.
    }

    public override void Enter()
    {
        base.Enter(); // Sets stage to UPDATE.
        _anim = us.GetComponent<Animator>();
        _lineShoot = us.GetComponent<LineRenderer>();
        _lineShoot.enabled = false;
    }
    public override void Update()
    {
        _anim.SetTrigger(_triggerAttack);
        navMeschAgent.destination = target.transform.position;
        _time += Time.deltaTime;
        if (us.tag==_tagForShoter)
        {
            Shoting();
        }
        PlayerHPManager.Instance.PlayerAttacked(_dmg);
        if ((target.transform.position - us.transform.position).magnitude > _distance)
        {
            nextState = new Pursue(us, target,navMeschAgent);
            stage = EVENT.EXIT;
        }
    }
    private void Shoting()
    {
        Debug.DrawLine(us.transform.position, target.transform.position, Color.red, _timeofVisibilityofShoot);
        if (!_visible && _time < _timebetwenShoot)
        {
            _visible = true;
            _pauze = true;
            _lineShoot.enabled = true;
            _lineShoot.positionCount = 2;
            _lineShoot.SetPosition(0, us.transform.position);
            _lineShoot.SetPosition(1, target.transform.position);
        }
        else
        if (_visible && _time > _timeofVisibilityofShoot)
        {
            _visible = false;
            _lineShoot.positionCount = 0;
            _lineShoot.enabled = _visible;
        }
        else if (_time > _timebetwenShoot)
        {
            _pauze = false;
            _time = 0;

        }
    }
    public override void Exit()
    {
        us.GetComponent<Animator>().SetTrigger(_esceped);
        base.Exit();
    }
}
