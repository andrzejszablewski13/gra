﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
public class Patroling : State
{
    private List<GameObject> _waypoints;
    private float _closestPoint = Mathf.Infinity, _temp;
    private int _iDNumber;
    public Patroling(GameObject _us, GameObject _target, NavMeshAgent _navMeshAgent)
                : base(_us, _target,_navMeshAgent)
    {
        name = STATE.PATROLING; // Set name of current state.
    }

    public override void Enter()
    {
        base.Enter(); // Sets stage to UPDATE.
        _waypoints = GameManagement.Instance.ReturnWaypoints();
        for (int i = 0; i < _waypoints.Count; i++)
        {
            _temp = Vector3.Distance(_waypoints[i].transform.position, us.gameObject.transform.position);
            if (_closestPoint>= _temp)
            {
                _closestPoint = _temp;
                navMeschAgent.destination = _waypoints[i].transform.position;
                _iDNumber = i;
            }
        }
        navMeschAgent.destination = _waypoints[1].transform.position;
    }
    public override void Update()
    {
        if ((target.transform.position - us.transform.position).magnitude < _distance)
        {
            nextState = new Pursue(us, target,navMeschAgent);
            stage = EVENT.EXIT;
        }else
        if ((us.transform.position - _waypoints[_iDNumber].transform.position).magnitude < _patrolDistance)
        {
            _iDNumber++;
            if(_iDNumber >= _waypoints.Count)
            {
                _iDNumber = 0;
            }
            navMeschAgent.destination = _waypoints[_iDNumber].transform.position;
        }
        
        
        
    }

    public override void Exit()
    {
        base.Exit();
    }
}
