﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OpenGate : MonoBehaviour
{
    // Start is called before the first frame update
    [SerializeField] private int _maxPoints = 4;
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if(ScoreManagement.Instance.GetPoint()>= _maxPoints)
        {
            this.gameObject.SetActive(false);
        }
    }
}
