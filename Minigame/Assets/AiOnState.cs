﻿using UnityEngine;
using UnityEngine.AI;
public class AiOnState : MonoBehaviour
{
    // Variables to handle what we need to send through to our state.

    [SerializeField]private GameObject _target;
    private NavMeshAgent _navmesch;
    private State _currentState;

    void Start()
    {
        _navmesch = this.gameObject.GetComponent<NavMeshAgent>();
        _currentState = new Idle2(this.gameObject,_target,_navmesch); // Create our first state.
        
    }

    void FixedUpdate()
    {
        _currentState = _currentState.Process(); // Calls Process method to ensure correct state is set.
    }
}
