﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManagement
{
    [SerializeField] private static List<GameObject> _waypoints;

    private static GameManagement _instance;
    public static GameManagement Instance
    {
        get
        {
            if(_instance == null)
            {
                _instance = new GameManagement();
                _waypoints = new List<GameObject>();
            }
            return _instance;
        }
    }

    private void Awake()
    {
        _instance = this;
    }
    public void AddPoint(GameObject _point)
    {
        _waypoints.Add(_point);
    }
    public List<GameObject> ReturnWaypoints()
    {
        return _waypoints;
    }
}
