﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScoreManagement : MonoBehaviour
{
    public int _currentScore;
    private int _coinValue = 1;

    private static ScoreManagement _instance;
    public static ScoreManagement Instance
    {
        get
        {
            if (_instance == null)
            {
                Debug.LogError("The game manager is null");
            }
            return _instance;
        }
    }

    private void Awake()
    {
        _instance = this;
    }

    public void AddPoint()
    {
        _currentScore += _coinValue;
        Debug.Log(_currentScore);
    }
    public int GetPoint()
    {
        return _currentScore;
    }
}
