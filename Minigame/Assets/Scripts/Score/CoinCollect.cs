﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CoinCollect : MonoBehaviour
{
    [SerializeField] private Collider _playerCollider;
    private MeshRenderer _coinMR;
    private Collider _coinCollider;

    private void Awake()
    {
        _coinMR = GetComponent<MeshRenderer>();
        _coinCollider = GetComponent<Collider>();
    }

    private void OnTriggerEnter(Collider player)
    {
        if (player == _playerCollider)
        {
            ScoreManagement.Instance.AddPoint();
            _coinCollider.enabled = false;
            _coinMR.enabled = false;
        }
    }
}
