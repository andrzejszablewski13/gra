﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class Trail3dOnMesch : MonoBehaviour
{
    // Start is called before the first frame update
    private MeshFilter _meshFilter ;
    private Mesh _originalMesh, _clonedMesh;
    private Vector3[] _vertices,_targetVerticles;
    private int[] _triangles;
    private bool _isCloned;
    private string _test;
    private float _distance=Mathf.Infinity;
    [SerializeField] private Transform _target;
    private MeshFilter _targetfilter;
    private List<int> _temp;
    void Start()
    {
        _targetfilter = _target.GetComponent<MeshFilter>();
        _targetVerticles = _targetfilter.sharedMesh.vertices;
        InitMesh();
        //_vertices[20] = _target.position-this.transform.position;
        //_vertices[21] = _target.position - this.transform.position;
        //_vertices[22] = _target.position - this.transform.position;
        //_vertices[23] = _target.position - this.transform.position;
        
        

    }
    private void FixedUpdate()
    {
        PullSimilarVertices(20, (_target.position + _targetVerticles[19]) - this.transform.position);//left corner
        PullSimilarVertices(21, (_target.position + _targetVerticles[18]) - this.transform.position);//left-up corner
        PullSimilarVertices(22, (_target.position + _targetVerticles[17]) - this.transform.position);//right-up corner
        PullSimilarVertices(23, (_target.position + _targetVerticles[16]) - this.transform.position);//right corner
        ShowLenght();

    }
    private void ShowLenght()
    {
        _test = null;
        for (int i = 0; i < _vertices.Length; i++)
        {
            _test += _vertices[i] + " ";
        }
        Debug.Log(_test);
    }

    // Update is called once per frame

    public void InitMesh()
    {
        
        _meshFilter = GetComponent<MeshFilter>();
        _originalMesh = _meshFilter.sharedMesh; //1
        _clonedMesh = new Mesh(); //2

        _clonedMesh.name = "clone";
        _clonedMesh.vertices = _originalMesh.vertices;
        _clonedMesh.triangles = _originalMesh.triangles;
        _clonedMesh.normals = _originalMesh.normals;
        _clonedMesh.uv = _originalMesh.uv;
        _meshFilter.mesh = _clonedMesh;  //3

        _vertices = _clonedMesh.vertices; //4
        _triangles = _clonedMesh.triangles;
        _isCloned = true; //5
        Debug.Log("Init & Cloned");
        
        
    }
    private List<int> FindRelatedVertices(Vector3 targetPt)
    {
        // list of int
        List<int> relatedVertices = new List<int>();

        int idx = 0;
        Vector3 pos;

        // loop through triangle array of indices
        for (int t = 0; t < _triangles.Length; t++)
        {
            // current idx return from tris
            idx = _triangles[t];
            // current pos of the vertex
            pos = _vertices[idx];
            // if current pos is same as targetPt
            if (pos == targetPt)
            {
                // add to list
                relatedVertices.Add(idx);
                // if find connected vertices
            }
        }
        // return compiled list of int
        return relatedVertices;
    }
    private void PullSimilarVertices(int index, Vector3 newPos)
    {
        Vector3 targetVertexPos = _vertices[index]; //1
        List<int> relatedVertices = FindRelatedVertices(targetVertexPos); //2
        foreach (int i in relatedVertices) //3
        {
            _vertices[i] = newPos;
        }
        _clonedMesh.vertices = _vertices; //4
        _clonedMesh.RecalculateNormals();
    }
}
