﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OnBotAttack : MonoBehaviour //bot musi mieć collider ontrigger
{
    [SerializeField] private Collider _playerCollider;
    private int _attackDamage = 5;

    private void OnTriggerEnter(Collider player)
    {
        if (player == _playerCollider)
        {
            PlayerHPManager.Instance.PlayerAttacked(_attackDamage);
        }
    }
}
