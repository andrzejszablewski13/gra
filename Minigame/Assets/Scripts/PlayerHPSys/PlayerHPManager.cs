﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerHPManager : MonoBehaviour //dodać na game managera
{
    private int _playerMaxHP = 25;
    private int _playerHP;
    private int _attackPower;
    private float _attackTimeDelay=2f;
    private bool _isPlayerAttacked;
    private bool _isPlayerAlive;

    private static PlayerHPManager _instance;
    public static PlayerHPManager Instance
    {
        get
        {
            if (_instance == null)
            {
                Debug.LogError("The game manager is null");
            }
            return _instance;
        }
    }

    private void Awake()
    {
        _instance = this;
        _isPlayerAttacked = false;
        _isPlayerAlive = true;
        _playerHP = _playerMaxHP;
    }

    public void PlayerAttacked(int attackPower)
    {
        _attackPower = attackPower;

        if (_playerHP <= 0)
        {
            _isPlayerAlive = false;
            Debug.Log("Player died");
        }

        if (!_isPlayerAttacked && _isPlayerAlive)
        {
            _isPlayerAttacked = true;
            StartCoroutine(AttackDelay());
        }
    }

    IEnumerator AttackDelay()
    {
        _playerHP -= _attackPower;
        yield return new WaitForSecondsRealtime(_attackTimeDelay);
        _isPlayerAttacked = false;
    }
}
