﻿using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.IO;
using UnityEditor;
using UnityEngine;

public class JsonMan : ScriptableSingleton<JsonMan>
{
    // Start is called before the first frame update
    private int[] _highScore = new int[10];
    private int[] _loadedChosenParts;
    private string[] _highScorename;
    public int[] HighScore
    {
        get
        {
            return _highScore;
        } 
        set
        {
            _highScore = value;
            Save();
        }
    }
    private void Load()
    {
        if(File.Exists(Application.dataPath + "/save.txt"))
        {
            string saveString=File.ReadAllText(Application.dataPath + "/save.txt");
            JSONSave saveobject = JsonUtility.FromJson<JSONSave>(saveString);
            _highScore = saveobject.bestscore;
        }
    }
    private void Save()
    {
        JSONSave saveobject=new JSONSave { bestscore = _highScore, chosenParts = _loadedChosenParts, bestscorename = _highScorename };
        string json = JsonUtility.ToJson(saveobject);
        File.WriteAllText(Application.dataPath + "/save.txt", json);
    }
}
