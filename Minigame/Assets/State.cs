﻿
using UnityEngine;
using UnityEngine.AI;
public class State
{
    // 'States' that the NPC could be in.
    public enum STATE
    {
        IDLE, PURSUE,PATROLING,ATTACK
    };

    // 'Events' - where we are in the running of a STATE.
    public enum EVENT
    {
        ENTER, UPDATE, EXIT
    };

    public STATE name; // To store the name of the STATE.
    protected EVENT stage; // To store the stage the EVENT is in.
    protected GameObject us; // To store the NPC game object.
    protected GameObject target; // To store the transform of the target.
    protected State nextState; // This is NOT the enum above, it's the state that gets to run after the one currently running.
    protected float _distance = 5f;
    protected float _patrolDistance = 5f;
    protected float _multiplayerForShoter = 2f;
    protected string _tagForShoter = "Shoting";
    protected NavMeshAgent navMeschAgent;
    //rest data for AI

    public State(GameObject _us, GameObject _target, NavMeshAgent _navMeshAgent)
    {
        us = _us;
        navMeschAgent = _navMeshAgent;
        stage = EVENT.ENTER;
        target = _target;
        if(us.tag== _tagForShoter)
        {
            _patrolDistance *= _multiplayerForShoter;
        }
    }

    // Phases as you go through the state.
    public virtual void Enter() { stage = EVENT.UPDATE; } // Runs first whenever you come into a state and sets the stage to whatever is next, so it will know later on in the process where it's going.
    public virtual void Update() { stage = EVENT.UPDATE; } // Once you are in UPDATE, you want to stay in UPDATE until it throws you out.
    public virtual void Exit() { stage = EVENT.EXIT; } // Uses EXIT so it knows what to run and clean up after itself.

    // The method that will get run from outside and progress the state through each of the different stages.
    public State Process()
    {
        if (stage == EVENT.ENTER) Enter();
        if (stage == EVENT.UPDATE) Update();
        if (stage == EVENT.EXIT)
        {
            Exit();
            return nextState; // Notice that this method returns a 'state'.
        }
        return this; // If we're not returning the nextState, then return the same state.
    }
   
}
// Constructor for Idle state.


