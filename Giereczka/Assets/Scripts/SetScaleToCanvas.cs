﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SetScaleToCanvas : MonoBehaviour
{
    // Start is called before the first frame update
    private RectTransform _canvTransform, _thisTransform;
    void Awake()
    {
        _canvTransform = this.GetComponentInParent<Canvas>().gameObject.GetComponent<RectTransform>();
        _thisTransform = this.GetComponent<RectTransform>();
        _thisTransform.localScale = _canvTransform.sizeDelta;
    }
}

