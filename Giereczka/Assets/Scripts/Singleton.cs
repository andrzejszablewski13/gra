﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Singleton : MonoBehaviour//singleton for points-probably soon deleted
{
    // Start is called before the first frame update
    private static GameObject _text;
    private void Awake()
    {
        _text = this.gameObject;
    }

    // Update is called once per frame
    public static GameObject ReturnText()
    {
        return _text;
    }
}
