using UnityEngine;

public class DriftCamera : MonoBehaviour
{
    //camera move-believe dont need explenation
    public float _smoothing = 6f;
    public Transform _lookAtTarget;//on player car
    public Transform _positionTarget;//starting camera position
    public bool setActive=false;
    private void FixedUpdate ()
    {
        if(setActive)
            UpdateCamera ();
    }
    private void UpdateCamera ()
    {
        {
            transform.position = Vector3.Lerp(transform.position, _positionTarget.position, Time.deltaTime * _smoothing);//smoth camera follow
            transform.LookAt(_lookAtTarget);
        }
    }
}
