﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ActivateAI : MonoBehaviour//activate selected numberr of AI
{
    [SerializeField] private GameObject[] _aiCars;
    void Start()
    {        
        for (int i = 0; i < _aiCars.Length; i++)
        {
            _aiCars[i].SetActive(false);
        }
        if (PlayerPrefs.HasKey("numberOfAi") && PlayerPrefs.GetInt("numberOfAi") > 0)
        {
            Debug.Log(PlayerPrefs.GetInt("numberOfAi"));
            for (int i = 0; i < PlayerPrefs.GetInt("numberOfAi"); i++)
            {
                if (i < _aiCars.Length)
                {
                    _aiCars[i].SetActive(true);
                    Debug.Log("test");
                }
                else
                    PlayerPrefs.SetInt("numberOfAi", i);
                Debug.Log(PlayerPrefs.GetInt("numberOfAi"));
            }
        }
    }
}
