﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class VehicleDecideButtons : MonoBehaviour//set car that will be played
{
    [SerializeField] private Button[] _buttons;
    [SerializeField] private Slider _slider;
    [SerializeField] private GameObject  _startMenu,_goback;
    private int _indexOfNextScene = 1;
    // Start is called before the first frame update
    void Start()
    {
        _buttons[0].onClick.AddListener(() => SetCat(0));
        _buttons[1].onClick.AddListener(() => SetCat(1));
        _buttons[2].onClick.AddListener(() => SetCat(2));
        _buttons[3].onClick.AddListener(() => SetCat(3));//dont know how made it shorted
        _goback.GetComponent<Button>().onClick.AddListener(GoBack);
        _slider.onValueChanged.AddListener(SetNumberOfAi);
        PlayerPrefs.SetInt("numberOfAi",0);
    }
    private void GoBack()
    {
        _startMenu.SetActive(true);
        this.gameObject.SetActive(false);
    }
    private void SetCat(int i)
    {
        PlayerPrefs.SetInt("decidedCar", i);
        SceneManager.LoadScene(_indexOfNextScene);
    }
    private void SetNumberOfAi(float i)
    {
        PlayerPrefs.SetInt("numberOfAi", (int)i);
    }
}
