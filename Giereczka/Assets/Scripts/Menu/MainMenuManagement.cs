﻿using UnityEngine;

public class MainMenuManagement : MonoBehaviour
{
    private void Awake()
    {
        if (SystemInfo.deviceType == DeviceType.Handheld)
        {
            Screen.orientation = ScreenOrientation.Landscape;
        }
    }
}