﻿using UnityEngine;

public class VehicleManager : MonoBehaviour//select car decided by player and set camera on it
{
    [SerializeField] private GameObject[] _vehicle;
    [SerializeField] private DriftCamera _camera;
    private Transform _temp;
    private int _camRigIndex = 5;
    private int _camLookAtTargetIndex = 0;
    private int _camPositionIndex = 1;

    private void Awake()
    {
        for (int i = 0; i < _vehicle.Length; i++)
        {
            _vehicle[i].SetActive(false);
        }
        try
        {
            _vehicle[PlayerPrefs.GetInt("decidedCar")].SetActive(true);
            _temp = _vehicle[PlayerPrefs.GetInt("decidedCar")].transform.GetChild(_camRigIndex);
        }
        catch
        {
            _vehicle[0].SetActive(true);
            _temp = _vehicle[0].transform.GetChild(_camRigIndex);
        }     

        _camera._lookAtTarget = _temp.GetChild(_camLookAtTargetIndex);
        _camera._positionTarget = _temp.GetChild(_camPositionIndex);
        _camera.setActive = true;
    }
}
