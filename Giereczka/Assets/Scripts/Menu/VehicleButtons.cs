﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class VehicleButtons : MonoBehaviour
{
    [SerializeField] private Button _v1Button, _v2Button, _v3Button, _v4Button;
    [SerializeField] private Button _backButton;
    [SerializeField] private Canvas _menuCanvas, _mainUICanvas;

    private int _mainMenuScene = 0;

    public delegate void VehicleChosen();
    public static event VehicleChosen playerChoseV1, playerChoseV2, playerChoseV3, playerChoseV4;

    private void Awake()
    {
        _v1Button.onClick.AddListener(ChooseVehicleOne);
        _v2Button.onClick.AddListener(ChooseVehicleTwo);
        _v3Button.onClick.AddListener(ChooseVehicleThree);
        _v4Button.onClick.AddListener(ChooseVehicleFour);
        _backButton.onClick.AddListener(GoBack);
    }

    private void ChooseVehicleOne()
    {
        playerChoseV1();
        SwitchMenus();
    }

    private void ChooseVehicleTwo()
    {
        playerChoseV2();
        SwitchMenus();
    }

    private void ChooseVehicleThree()
    {
        playerChoseV3();
        SwitchMenus();
    }

    private void ChooseVehicleFour()
    {
        playerChoseV4();
        SwitchMenus();
    }

    private void SwitchMenus()
    {
        _menuCanvas.gameObject.SetActive(false);
        _mainUICanvas.gameObject.SetActive(true);
    }

    private void GoBack()
    {
        SceneManager.LoadScene(_mainMenuScene);
    }
}
