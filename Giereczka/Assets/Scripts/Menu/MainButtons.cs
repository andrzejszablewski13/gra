﻿using UnityEngine;
using UnityEngine.UI;

public class MainButtons : MonoBehaviour
{
    [SerializeField] private Button _playButton;
    [SerializeField] private Button _exitButton;
    [SerializeField] private Button _multiButton;
    [SerializeField] private Button _settingButton;
    [SerializeField] private GameObject _backGround,_decideCar,_canvas,_multi,_settings;

   private void Awake()
    {
        _backGround.GetComponent<RectTransform>().sizeDelta = _canvas.GetComponent<RectTransform>().sizeDelta;
        _playButton.onClick.AddListener(Play);
        _exitButton.onClick.AddListener(Exit);
        _multiButton.onClick.AddListener(StartMultiMenu);
        _settingButton.onClick.AddListener(Settings);
    }

    private void Play()
    {
        _decideCar.SetActive(true); 
        this.gameObject.SetActive(false);
    }
    private void StartMultiMenu()
    {
        _multi.SetActive(true);
        this.gameObject.SetActive(false);
    }
    private void Settings()
    {
        _settings.SetActive(true);
        this.gameObject.SetActive(false);
    }
    private void Exit()
    {
        Application.Quit();
    }
}
