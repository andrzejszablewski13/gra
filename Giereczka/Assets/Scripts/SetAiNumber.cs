﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SetAiNumber : MonoBehaviour
{
    // Start is called before the first frame update
    [SerializeField][Range(0f,4f)] private int _aiNumber = 0;
    [SerializeField] private bool _bonusactiveSwitch = false;
    void Awake()
    {
        if (_bonusactiveSwitch)
        {
            PlayerPrefs.SetFloat("numberOfAi", _aiNumber);
        }
    }
}
