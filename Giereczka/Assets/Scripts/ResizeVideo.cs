﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Video;

public class ResizeVideo : MonoBehaviour
{
    // Start is called before the first frame update
    private VideoPlayer _player;
    private VideoClip _video;
    [SerializeField] private RectTransform _canvas;
    private float _height, _width;
    void Start()
    {
        _player = this.gameObject.GetComponent<VideoPlayer>();
        _video = _player.clip;
        
    }
    private void Update()
    {
        _height = _canvas.rect.height/100;
        _width = _canvas.rect.width / 100;
        this.transform.localScale = new Vector3(_width, _height, 0);
    }


}
