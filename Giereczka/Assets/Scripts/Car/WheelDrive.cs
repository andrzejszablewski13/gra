﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

[Serializable]
public enum DriveType
{
	RearWheelDrive,
	FrontWheelDrive,
	AllWheelDrive
}
public enum Controls
{
    Horizontal, Vertical, HandBar
}

public class WheelDrive : MonoBehaviour
{
    public delegate void SpeedBoostUsed();
    public static event SpeedBoostUsed SpeedBoostUsedEvent;

    [Tooltip("Maximum steering angle of the wheels")]
    [SerializeField] protected float _maxAngle = 45f;
    [Tooltip("Maximum torque applied to the driving wheels")]
    [SerializeField] protected float _maxTorque = 3000f;
	[Tooltip("Maximum brake torque applied to the driving wheels")]
    [SerializeField] protected float _brakeTorque = 300000f;
	[Tooltip("If you need the visual wheels to be attached automatically, drag the wheel shape here.")]
    [SerializeField] protected GameObject _wheelShape,_backWheelShape;

    [Tooltip("The vehicle's speed when the physics engine can use different amount of sub-steps (in m/s).")]
    [SerializeField] protected float _criticalSpeed = 5f;
	[Tooltip("Simulation sub-steps when the speed is above critical.")]
    [SerializeField] protected int _stepsBelow = 5;
	[Tooltip("Simulation sub-steps when the speed is below critical.")]
    [SerializeField] protected int _stepsAbove = 1;

	[Tooltip("The vehicle's drive type: rear-wheels drive, front-wheels drive or all-wheels drive.")]
	[SerializeField] protected DriveType _driveType=DriveType.AllWheelDrive;

    protected WheelCollider[] _mWheels;
    protected float _angle, _torque, _handBrake;
    protected Quaternion _q;
    protected Vector3 _p;
    [SerializeField]
    private string _nameOfLayer;
    public  string NameOfLayer 
    { get
        {
            return _nameOfLayer;
        }
        set 
        {
            _nameOfLayer = value;
            _trail.nameOfLayer = value;
            this.gameObject.layer =LayerMask.NameToLayer(value);
            foreach (Transform trans in this.gameObject.GetComponentsInChildren<Transform>(true))//set layer for all children
            {
                trans.gameObject.layer = LayerMask.NameToLayer(NameOfLayer);
            }
        }
    }
    [SerializeField] [Range (10000f, 200000f)] protected float _speedBoostMultiplier;
    protected float _tempInputLocation;
    [Range(0.5f, 5f)] public float _scaleOfSegment = 1f;
    protected Rigidbody _rb;
    private float _actualSpeed;
    private Vector3[] _positionSafe;
    [SerializeField] protected Trail3D _trail;
    Dictionary<string, GameObject> _thisCar;
   [SerializeField] public float ActualSpeed { get => _actualSpeed;}

    // Find all the WheelColliders down in the hierarchy.
    void Start()
	{
        BeforeScriptStart();
    }
    private void OnEnable()
    {
        PickupButtonManager.SpeedBoostPickupDelegateEvent += Booster;
    }

    private void OnDisable()
    {
        PickupButtonManager.SpeedBoostPickupDelegateEvent -= Booster;
    }

    protected void BeforeScriptStart()
    {
        _rb = this.GetComponent<Rigidbody>();
        CarSingleton.Singleton.AddCar(this.name, this.gameObject);
        _mWheels = this.GetComponentsInChildren<WheelCollider>();//find place for wheel

        for (int i = 0; i < _mWheels.Length; ++i)//add wheels
        {
            WheelCollider wheel = _mWheels[i];

            // Create wheel shapes only when needed.
            if(_backWheelShape!=null && (_mWheels[i].gameObject.name== "a1l" || _mWheels[i].gameObject.name == "a1r"))
            {
                GameObject ws = Instantiate(_backWheelShape);
                ws.transform.parent = wheel.transform;
            }
            else
            if (_wheelShape != null)
            {
                GameObject ws = Instantiate(_wheelShape);
                ws.transform.parent = wheel.transform;
            }
        }
        this.gameObject.layer = LayerMask.NameToLayer(NameOfLayer);//set proper layer
        foreach (Transform trans in this.gameObject.GetComponentsInChildren<Transform>(true))//set layer for all children
        {
            trans.gameObject.layer = LayerMask.NameToLayer(NameOfLayer); ;
        }
        this.gameObject.GetComponentInChildren<Trail3D>().nameOfLayer = this.NameOfLayer;
        this.gameObject.GetComponentInChildren<Trail3D>()._scaleOfSegment = this._scaleOfSegment;
        _positionSafe = new Vector3[2];
        _thisCar = new Dictionary<string, GameObject>(CarSingleton.Singleton.GiveCar(this.name));
        _positionSafe[0] = _thisCar[CarSingleton.Names.LeftCorner.ToString()].transform.localPosition;
        _positionSafe[1] = _thisCar[CarSingleton.Names.RightCorner.ToString()].transform.localPosition;
        
    }

    protected void Booster() //do SpeedBoost, przyspieszanie pojazdu
    {
        _rb.AddForce(transform.forward * _speedBoostMultiplier);
        SpeedBoostUsedEvent(); //sygnał do ButtonManager
    }

    void Update()
	{
        MovingSystem();
    }
    protected virtual void MovingSystem()//control system for car-not our code
    {
        _actualSpeed= _rb.velocity.magnitude;
        if(_actualSpeed>=5)
        {
            _thisCar[CarSingleton.Names.LeftCorner.ToString()].transform.localPosition = _positionSafe[0] * _actualSpeed / 5;
            _thisCar[CarSingleton.Names.RightCorner.ToString()].transform.localPosition = _positionSafe[1] * _actualSpeed / 5;
        }
        else if(_thisCar[CarSingleton.Names.LeftCorner.ToString()].transform.localPosition != _positionSafe[0])
        {
            _thisCar[CarSingleton.Names.LeftCorner.ToString()].transform.localPosition = _positionSafe[0];
            _thisCar[CarSingleton.Names.RightCorner.ToString()].transform.localPosition = _positionSafe[1];

        }
        MovingSystemCore();
    }
    protected void MovingSystemCore()
    {
        foreach (WheelCollider wheel in _mWheels)
        {
            // A simple car where front wheels steer while rear ones drive.
            if (wheel.transform.localPosition.z > 0)
                wheel.steerAngle = _angle;

            if (wheel.transform.localPosition.z < 0)
            {
                wheel.brakeTorque = _handBrake;
            }

            if (wheel.transform.localPosition.z < 0 && _driveType != DriveType.FrontWheelDrive)
            {
                wheel.motorTorque = _torque;
            }

            if (wheel.transform.localPosition.z >= 0 && _driveType != DriveType.RearWheelDrive)
            {
                wheel.motorTorque = _torque;
            }


            // Update visual wheels if any.
            if (_wheelShape)
            {
                wheel.GetWorldPose(out _p, out _q);

                // Assume that the only child of the wheelcollider is the wheel shape.
                Transform shapeTransform = wheel.transform.GetChild(0);

                if (wheel.name == "a0l" || wheel.name == "a1l" || wheel.name == "a2l")
                {
                    shapeTransform.rotation = _q * Quaternion.Euler(0, 180, 0);
                    shapeTransform.position = _p;
                }
                else
                {
                    shapeTransform.position = _p;
                    shapeTransform.rotation = _q;
                }
            }
        }
    }
    protected virtual void ControlsSystem()//player controls-our code
    {
        //code set in children
    }
   
}
