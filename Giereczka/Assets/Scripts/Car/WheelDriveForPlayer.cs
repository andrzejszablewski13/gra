﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WheelDriveForPlayer : WheelDrive//like name say
{
    // Start is called before the first frame update
    void Start()
    {
        //CarSingleton.Singleton.AddCar(this.name, this.gameObject);
        base.BeforeScriptStart();
    }

    // Update is called once per frame
    void Update()
    {
        this.MovingSystem();
    }
    protected override void MovingSystem()
    {
        _mWheels[0].ConfigureVehicleSubsteps(_criticalSpeed, _stepsBelow, _stepsAbove);
        if (SystemInfo.deviceType != DeviceType.Handheld)//seting control system handheld/PC
        {
             this.ControlsSystem();
        }
        else
        {
            SpeedUpOnAndroidDevice();
            if ((Input.acceleration.magnitude != 0 && PlayerPrefs.GetInt(SettingsMenu.NameOfPlayerPref.setControlers.ToString()) == (int)SettingsMenu.NameForSettingControlers.Default) ||
                       PlayerPrefs.GetInt(SettingsMenu.NameOfPlayerPref.setControlers.ToString()) == (int)SettingsMenu.NameForSettingControlers.Accelerator)
            {
                RotateByAccelerometer();//on accelorometer-rotate handheld
            }
            else 
            {
                AndroidControls();// on buttons
            }


            }
        base.MovingSystem();
    }
    private void AndroidControls()
    {
        _angle = _maxAngle * LRButtons._value;
    }

    protected override void ControlsSystem()//players Controls on PC
    {
        _angle = _maxAngle * Input.GetAxis(Controls.Horizontal.ToString());
        _tempInputLocation = Input.GetAxis(Controls.Vertical.ToString());
        SlowOrSpeedUp();

        _handBrake = Input.GetButton(Controls.HandBar.ToString()) ? _brakeTorque : 0;
    }
    public void SpeedUpOnAndroidDevice()//non stop speeding up on handheld
    {
        _tempInputLocation = 0.9f;
        _handBrake = 0f;
        SlowOrSpeedUp();

    }
    private void RotateByAccelerometer()//rotate by accelerometer on handheld
    {
        _angle = _maxAngle * Input.acceleration.x;
    }
    private void SlowOrSpeedUp()//slowing when not speeding
    {
        if (_tempInputLocation > 0)
        {
            _torque = _maxTorque * _tempInputLocation;
        }
        else if (_tempInputLocation == 0)
        {
            _torque = _tempInputLocation;
            _handBrake = _brakeTorque / 10;
        }
        else if (_tempInputLocation < 0)
        {
            _handBrake = _brakeTorque / 2;
        }
    }
}
