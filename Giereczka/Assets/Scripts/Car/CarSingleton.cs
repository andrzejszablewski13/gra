﻿using System.Collections.Generic;
using UnityEngine;

public sealed class CarSingleton
{
    public enum Names : byte
    {
        CenterFront, CenterBack, LeftCorner, RightCorner, ForAI,ThisCar, Rest
    }
    private GameObject _temporaly;
    private static CarSingleton _instance;
    private Dictionary<string,Dictionary<string,GameObject>> _cars;
    private List<string> _listOfCars;
    public static CarSingleton Singleton//get singleton, if not exist create it
        {
        get {
            if (_instance == null)
            {
                _instance = new CarSingleton
                {
                    _cars = new Dictionary<string, Dictionary<string, GameObject>>(),
                    _listOfCars = new List<string>()
                };
            }
            return _instance;       
        }
        }
    public void AddCar(string name,GameObject _thisCar)//add car (gameobject) to singleton
    {
        _cars.Add(name,new Dictionary<string, GameObject>());
        _cars[name].Add(Names.ThisCar.ToString(), _thisCar);
        _listOfCars.Add(name);
        AddForAI(name);
    }
    private void AddForAI(string name)//find and create refrence 'ForAI' object in car gameobject
    {
        if (_cars[name].TryGetValue(Names.ThisCar.ToString(), out _temporaly))
        {
            for (int i = 0; i < _temporaly.transform.childCount; i++)
            {
                if(_temporaly.transform.GetChild(i).name == Names.Rest.ToString())
                {
                    _cars[name].Add(Names.Rest.ToString(), _temporaly.transform.GetChild(i).gameObject);
                }
                else
                if (_temporaly.transform.GetChild(i).name == Names.ForAI.ToString())
                {
                    _cars[name].Add(Names.ForAI.ToString(), _temporaly.transform.GetChild(i).gameObject);
                }
            }
            AddAntene(name);
        }
    }
    private void AddAntene(string name)//find and create reference for each antene in 'ForAI'
    {
        if (_cars[name].TryGetValue(Names.ForAI.ToString(), out _temporaly))
        {
            for (int i = 0; i < _temporaly.transform.childCount; i++)
            {
                switch (_temporaly.transform.GetChild(i).name)
                {
                    case "CenterFront":
                        _cars[name].Add(Names.CenterFront.ToString(), _temporaly.transform.GetChild(i).gameObject);
                        break;
                    case "CenterBack":
                        _cars[name].Add(Names.CenterBack.ToString(), _temporaly.transform.GetChild(i).gameObject);
                        break;
                    case "LeftCorner":
                        _cars[name].Add(Names.LeftCorner.ToString(), _temporaly.transform.GetChild(i).gameObject);
                        break;
                    case "RightCorner":
                        _cars[name].Add(Names.RightCorner.ToString(), _temporaly.transform.GetChild(i).gameObject);
                        break;
                    default:
                        break;
                }
            }
        }
    }//get position of antens of one enemy
    public Dictionary<string, GameObject> GiveCar(string name)//give script all collected data of selected car
    {
        return _cars[name];
    }
    public List<string> GiveListOfCars()//give list of all cars in singleton
    {
        return _listOfCars;
    }
    public void ClearSingleton()
    {
        _instance = null;
    }
    public GameObject ReceiveRestCar(string name)
    {
        return _cars[name][Names.Rest.ToString()];
    }
}
