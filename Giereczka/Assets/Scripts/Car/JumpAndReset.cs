﻿using UnityEngine;

public class JumpAndReset : MonoBehaviour
{
    private Rigidbody _Rb;
    private Transform _carTransform;
    [SerializeField] private int _forcePower = 10000;

    public delegate void JumpUsed();
    public static event JumpUsed jumpUsed;

    void Awake()
    {
        _Rb = this.gameObject.GetComponent<Rigidbody>();
        _carTransform= this.gameObject.GetComponent<Transform>();
    }

    private void OnEnable()
    {
        PickupButtonManager.JumpPickupDelegateEvent += Jump;
    }

    private void OnDisable()
    {
        PickupButtonManager.JumpPickupDelegateEvent -= Jump;
    }

    //void Update()
    //{
    //    if (Input.GetKeyDown(KeyCode.C))//reset position-if fall on wrong side
    //    {
    //    _carTransform.SetPositionAndRotation(new Vector3(_carTransform.position.x, _carTransform.position.y + 1, _carTransform.position.z), new Quaternion(0, 0,0, 0));
    //    }
    //}
    void Jump()
    {
        _Rb.AddForce(Vector3.up * _forcePower, ForceMode.Impulse);
        jumpUsed();
    }
}
