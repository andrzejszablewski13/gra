﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Trail3D : MonoBehaviour//create trail by modyfiing mesch
{
    private Transform _trailLocation;// place where trail 3d will be generated
    private Rigidbody _ourVelocityRig;
    private int _numberOfSegments = 500;//number of segments pre-generated for 3d trail
    private readonly float _segmentMusltiplayerPesVisSecond = 20;
    [SerializeField] private float _timeToHide=25;//how long segment will live on scene
    [SerializeField] private bool _selfColision=false;//car lollide wchich its own segments?(true=yes)
    [SerializeField] private Transform _segmentLocation;//locatino where segments will be stored (as children of gameobject)
    [SerializeField] private GameObject _trailSegmentPreFab;//as name say
    private GameObject _freeSegments, _activeSegment, _visibleSegments,_temp,_tempActiveTrail;//for sorting swegments ("folders' and last for temp access)
    private GameObject[] _segments;//all segments
    public string nameOfLayer;//as name say
    private readonly string  _killAllName = "KillAll";
    private readonly string[] _nameOfFolders = new string[3] { "FreeSegemts", "ActiveSegment", "VisibleSegments" };
    [Range (0.5f,5f)] public float _scaleOfSegment=1f;
    private readonly float _distancebeetwensegments = 2f;


    public delegate void EraserUsed();
    public static event EraserUsed EraserUsedEvent;

    private MeshFilter _meshFilter;
    private Mesh _originalMesh, _clonedMesh;
    private Vector3[] _vertices, _targetVerticles,_baseVericles;
    private int[] _triangles;
    private Vector3 _zeroRotateOurPosition;
    private MeshCollider _trailcollither;
    private readonly float[] _sizeofMEsch=new float[4] { 0.5f, -0.5f, 0.1f, -0.1f };
    private readonly string _name = "TrailClone";
    private bool _isRotated = false;
    [SerializeField] private Material _trailMaterial;
    enum VerticlesOfCube//forward(first option) is on blue arrow-rest add later
    {
        forwardDownLeft=0,forwardDownRight=1,forwardUpLeft=2,forwardUpRight=3,
        backDownLeft=6,backDownRight=7,backUpLeft=10,backUpRight=11
    }

    private void OnEnable()//pre-generate segments and place them in 'folder' freesegments
    {
        PickupButtonManager.EraserPickupDelegateEvent += EraseTrail;
        _numberOfSegments = (int)(_timeToHide * _segmentMusltiplayerPesVisSecond);
        _trailLocation = this.gameObject.GetComponent<Transform>();
        _ourVelocityRig = this.gameObject.GetComponentInParent<Rigidbody>();
        _freeSegments = new GameObject
        {
            name = _nameOfFolders[0]
        };
        _freeSegments.transform.SetParent(_segmentLocation);
        _activeSegment = new GameObject
        {
            name = _nameOfFolders[1]
        };
        _activeSegment.transform.SetParent(_segmentLocation);
        _visibleSegments = new GameObject
        {
            name = _nameOfFolders[2]
        };
        _visibleSegments.transform.SetParent(_segmentLocation);
        _segments = new GameObject[_numberOfSegments];
        for (int i = 0; i < _numberOfSegments; i++)
        {
            _segments[i] = Instantiate(_trailSegmentPreFab, Vector3.zero, Quaternion.identity, _freeSegments.transform);
            _segments[i].layer = LayerMask.NameToLayer(nameOfLayer);
            _segments[i].SetActive(false);
        }
        _temp = _freeSegments.transform.GetChild(0).gameObject;
        _temp.transform.SetParent(_activeSegment.transform);
        InitMesh(this.GetComponent<MeshFilter>());
        for (int i = 0; i < _vertices.Length; i++)//here set size and shape of trail
        {
            if (_vertices[i].x == _sizeofMEsch[0])
            {
                _vertices[i] = new Vector3(_sizeofMEsch[2], _vertices[i].y, _vertices[i].z);
            }
            else if (_vertices[i].x == _sizeofMEsch[1])
            {
                _vertices[i] = new Vector3(_sizeofMEsch[3], _vertices[i].y, _vertices[i].z);
            }
        }
        _clonedMesh.vertices = _vertices; //4
        _clonedMesh.RecalculateNormals();
        _baseVericles = _vertices;
    }
    private void OnDisable()
    {
        PickupButtonManager.EraserPickupDelegateEvent -= EraseTrail;
        for (int i = _segmentLocation.transform.childCount; i >= 0; i--)
        {
            Destroy(_segmentLocation.transform.GetChild(i).gameObject);
        }
    }
    public void DeleteTrail()//as name say
    {
        for (int i = _visibleSegments.transform.childCount - 1; i >= 0; i--)
        {
            _temp = _visibleSegments.transform.GetChild(i).gameObject;
            _temp.transform.SetParent(_freeSegments.transform);
            _temp.SetActive(false);
        }
        for (int i = _activeSegment.transform.childCount - 1; i >= 0; i--)
        {
            _temp = _activeSegment.transform.GetChild(i).gameObject;
            _temp.transform.SetParent(_freeSegments.transform);
            _temp.SetActive(false);
        }
    }
    void FixedUpdate()
    {
        {
            TrailSegmentGeneration();
            TrailMeschManipulator();
        }
    }
    private void TransformUpdate()
    {
        _targetVerticles = new Vector3[_sizeofMEsch.Length];
        for (int i = 0; i < _targetVerticles.Length; i++)
        { 
             _targetVerticles[i] = _zeroRotateOurPosition + _baseVericles[i]; 
        }
        
    }
    private void TrailMeschManipulator()//manipulator mesch of each trail segment
    {
        _temp = _activeSegment.transform.GetChild(0).gameObject;
        _zeroRotateOurPosition = this.transform.position;
        TransformUpdate();
        
        _meshFilter = _temp.GetComponent<MeshFilter>();
            InitMesh(_meshFilter, _name);
        
        for (int i = 0; i < _targetVerticles.Length; i++)
        {
            if (_ourVelocityRig.velocity.z >= 0)
            {
                PullSimilarVertices(i, _targetVerticles[i] - _temp.transform.position);

            }
            else
            {
                switch (i)
                {
                    case 0:
                        PullSimilarVertices((int)VerticlesOfCube.backDownLeft, _targetVerticles[i] - _temp.transform.position);
                        break;
                    case 1:
                        PullSimilarVertices((int)VerticlesOfCube.backDownRight, _targetVerticles[i] - _temp.transform.position);
                        break;
                    case 2:
                        PullSimilarVertices((int)VerticlesOfCube.backUpLeft, _targetVerticles[i] - _temp.transform.position);
                        break;
                    case 3:
                        PullSimilarVertices((int)VerticlesOfCube.backUpRight, _targetVerticles[i] - _temp.transform.position);
                        break;
                    default:
                        break;
                }
            }
        }
        EndAfterPullSimilarVertices();
    }

    private void EraseTrail() //do boosta Eraser, "usuwa" stworzone dotychczas traile
    {
        DeleteTrail();
        EraserUsedEvent(); //wysyła sygnał do klasy ButtonManager
    }

    private void TrailSegmentGeneration()//generate segments on scene, give them proper layer and move it to proper 'folder'
    {
        if((_activeSegment.transform.childCount == 0)//_activeSegments-segment closest to car
            ||(Vector3.Distance(_activeSegment.transform.GetChild(0).gameObject.transform.position, _trailLocation.position)>=_distancebeetwensegments) 
            && (_freeSegments.transform.childCount > 0)
            )
        {

            
            if (_activeSegment.transform.childCount > 0)
            {
                if (_tempActiveTrail != null)
                {
                    _tempActiveTrail.GetComponent<MeshCollider>().convex = true;

                    if (_selfColision)
                    {
                        _tempActiveTrail.layer = LayerMask.NameToLayer(_killAllName);
                    }
                }
                _tempActiveTrail = _activeSegment.transform.GetChild(0).gameObject;
                _meshFilter = _tempActiveTrail.GetComponent<MeshFilter>();//make trail look like one line
                InitMesh(_meshFilter, _name);
                _trailcollither=_tempActiveTrail.GetComponent<MeshCollider>();
                _trailcollither.sharedMesh = _clonedMesh;
                _targetVerticles = _vertices;
                _temp = _freeSegments.transform.GetChild(0).gameObject;
                _temp.transform.position = _trailLocation.position;
                _meshFilter = _temp.GetComponent<MeshFilter>();
                InitMesh(_meshFilter, _name);
                
                if (_ourVelocityRig.velocity.z > 0) //connect segments of trail
                {
                    if (_isRotated == true)
                    {
                        if(_visibleSegments.transform.childCount>2)
                        RepairTrailOnVelZChange(true);
                    }
                   
                        PullSimilarVertices((int)VerticlesOfCube.backDownLeft, (_tempActiveTrail.transform.position + _targetVerticles[(int)VerticlesOfCube.forwardDownLeft]) - _temp.transform.position);//left corner
                        PullSimilarVertices((int)VerticlesOfCube.backDownRight, (_tempActiveTrail.transform.position + _targetVerticles[(int)VerticlesOfCube.forwardDownRight]) - _temp.transform.position);//left-up corner
                        PullSimilarVertices((int)VerticlesOfCube.backUpLeft, (_tempActiveTrail.transform.position + _targetVerticles[(int)VerticlesOfCube.forwardUpLeft]) - _temp.transform.position);//right-up corner
                        PullSimilarVertices((int)VerticlesOfCube.backUpRight, (_tempActiveTrail.transform.position + _targetVerticles[(int)VerticlesOfCube.forwardUpRight]) - _temp.transform.position);//right corner
                    

                    _isRotated = false;
                }
                else
                {
                    if (_isRotated == false)
                    {
                        if (_visibleSegments.transform.childCount > 2)
                            RepairTrailOnVelZChange(false);
                    }
                    else
                    {
                        PullSimilarVertices((int)VerticlesOfCube.forwardDownLeft, (_tempActiveTrail.transform.position + _targetVerticles[(int)VerticlesOfCube.backDownLeft]) - _temp.transform.position);//left corner
                        PullSimilarVertices((int)VerticlesOfCube.forwardDownRight, (_tempActiveTrail.transform.position + _targetVerticles[(int)VerticlesOfCube.backDownRight]) - _temp.transform.position);//left-up corner
                        PullSimilarVertices((int)VerticlesOfCube.forwardUpLeft, (_tempActiveTrail.transform.position + _targetVerticles[(int)VerticlesOfCube.backUpLeft]) - _temp.transform.position);//right-up corner
                        PullSimilarVertices((int)VerticlesOfCube.forwardUpRight, (_tempActiveTrail.transform.position + _targetVerticles[(int)VerticlesOfCube.backUpRight]) - _temp.transform.position);//right corner

                    }
                    _isRotated = true;
                }
                EndAfterPullSimilarVertices();
                _activeSegment.transform.GetChild(0).gameObject.transform.SetParent(_visibleSegments.transform);

            }else
            {
                _temp = _freeSegments.transform.GetChild(0).gameObject;
                _temp.transform.position = _trailLocation.position;
            }
            _temp.transform.SetParent(_activeSegment.transform);
            if (_trailMaterial != null)
            { _temp.GetComponent<Renderer>().material = _trailMaterial; }
            _temp.SetActive(true);
            
            StartCoroutine(HideAfterTime(_timeToHide, _temp));
        }
    }
    GameObject _tempGO1;
    MeshFilter _tempMeschFilter;
    Vector3[] _tempVerticles;
    Vector3[] _savingvericles;
    int[] _savingTriangles;
    Mesh _meschSave;
    private void RepairTrailOnVelZChange(bool _zisLarger)//method to repair bug to close break in trail that create if z axis of velocity change to below orr above 0
    {
        _tempGO1 = _visibleSegments.transform.GetChild(_visibleSegments.transform.childCount - 1).gameObject;
        _meshFilter = _tempGO1.GetComponent<MeshFilter>();
        _tempMeschFilter= _tempActiveTrail.GetComponent<MeshFilter>();
        _tempVerticles= _tempMeschFilter.sharedMesh.vertices;
        _savingvericles = _vertices;
        _savingTriangles = _triangles;
        _meschSave = _clonedMesh;
        _clonedMesh = _meshFilter.sharedMesh;
        _vertices = _clonedMesh.vertices;
        _triangles = _clonedMesh.triangles;
        if (_zisLarger)
        {
            PullSimilarVertices((int)VerticlesOfCube.backDownLeft, (_tempActiveTrail.transform.position + _tempVerticles[(int)VerticlesOfCube.backDownLeft]) - _tempGO1.transform.position);//left corner
            PullSimilarVertices((int)VerticlesOfCube.backDownRight, (_tempActiveTrail.transform.position + _tempVerticles[(int)VerticlesOfCube.backDownRight]) - _tempGO1.transform.position);//left-up corner
            PullSimilarVertices((int)VerticlesOfCube.backUpLeft, (_tempActiveTrail.transform.position + _tempVerticles[(int)VerticlesOfCube.backUpLeft]) - _tempGO1.transform.position);//right-up corner
            PullSimilarVertices((int)VerticlesOfCube.backUpRight, (_tempActiveTrail.transform.position + _tempVerticles[(int)VerticlesOfCube.backUpRight]) - _tempGO1.transform.position);//right corner
        }
        else
        {
            PullSimilarVertices((int)VerticlesOfCube.forwardDownLeft, (_tempActiveTrail.transform.position + _tempVerticles[(int)VerticlesOfCube.forwardDownLeft]) - _tempGO1.transform.position);//left corner
            PullSimilarVertices((int)VerticlesOfCube.forwardDownRight, (_tempActiveTrail.transform.position + _tempVerticles[(int)VerticlesOfCube.forwardDownRight]) - _tempGO1.transform.position);//left-up corner
            PullSimilarVertices((int)VerticlesOfCube.forwardUpLeft, (_tempActiveTrail.transform.position + _tempVerticles[(int)VerticlesOfCube.forwardUpLeft]) - _tempGO1.transform.position);//right-up corner
            PullSimilarVertices((int)VerticlesOfCube.forwardUpRight, (_tempActiveTrail.transform.position + _tempVerticles[(int)VerticlesOfCube.forwardUpRight]) - _tempGO1.transform.position);//right corner

        }
        EndAfterPullSimilarVertices();
        _clonedMesh = _meschSave;
        _triangles = _savingTriangles;
        _vertices = _savingvericles;
    }
    private IEnumerator HideAfterTime(float waitTime, GameObject p)//if time of segment has expired
    {
        
            yield return new WaitForSeconds(waitTime);
            p.transform.SetParent(_freeSegments.transform);
        if (_selfColision)
        {
            p.layer = LayerMask.NameToLayer(nameOfLayer);
        }
        p.SetActive(false);

    }
    //this 4 method are not our
    private Vector3 RotatePointAroundPivot(Vector3 point, Vector3 pivot, Vector3 angles)
    {
        return Quaternion.Euler(angles) * (point - pivot) + pivot;
    }
    private void InitMesh(MeshFilter mesch, string name="clone")
    {
        _meshFilter = mesch;
        _originalMesh = _meshFilter.sharedMesh; //1
        _clonedMesh = new Mesh
        {
            name = name,
            vertices = _originalMesh.vertices,
            triangles = _originalMesh.triangles,
            normals = _originalMesh.normals,
            uv = _originalMesh.uv
        }; //2
        _meshFilter.mesh = _clonedMesh;  //3

        _vertices = _clonedMesh.vertices; //4
        _triangles = _clonedMesh.triangles;


    }
    int idx = 0;
    Vector3 pos;
    List<int> relatedVertices;
    private List<int> FindRelatedVertices(Vector3 targetPt)
    {
        // list of int
        relatedVertices = new List<int>();




        // loop through triangle array of indices
        for (int t = 0; t < _triangles.Length; t++)
        {
            // current idx return from tris
            idx = _triangles[t];
            // current pos of the vertex
            pos = _vertices[idx];
            // if current pos is same as targetPt
            if (pos == targetPt)
            {
                // add to list
                relatedVertices.Add(idx);
                // if find connected vertices
            }
        }
        // return compiled list of int
        return relatedVertices;
    }
    Vector3 targetVertexPos;
    private void PullSimilarVertices(int index, Vector3 newPos)
    {
        targetVertexPos = _vertices[index]; //1
        FindRelatedVertices(targetVertexPos); //2
        foreach (int i in relatedVertices) //3
        {
            _vertices[i] = newPos;

        }
        _clonedMesh.vertices = _vertices; //4
        
    }
    private void EndAfterPullSimilarVertices()
    {
        _clonedMesh.RecalculateNormals();
        _clonedMesh.vertices = _vertices;
    }



}
