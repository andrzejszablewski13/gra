﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WheelDriveForBots : WheelDrive//like name say
{
    // WheelDrive modyficated for bots
    private MovementControl controlSystem;
    void Start()
    {
        base.BeforeScriptStart();
        controlSystem = this.gameObject.GetComponent<MovementControl>();
        controlSystem.AIBegin();
        this.GetComponent<AIOnState>().enabled = true;

    }

    // Update is called once per frame
    void Update()
    {
        _mWheels[0].ConfigureVehicleSubsteps(_criticalSpeed, _stepsBelow, _stepsAbove);
        this.MovingSystem();
    }
    protected override void MovingSystem()
    {
        ControlsSystem();
        base.MovingSystem();
    }
    protected override void ControlsSystem()//geting rotation from AI
    {
        this._torque = this._maxTorque;
        //this._angle = controlSystem.VerticalAngleForWheel;
        if(controlSystem.VerticalAngleForWheel<= _maxAngle)
        {
            this._angle = controlSystem.VerticalAngleForWheel;
        }
        else
        {
            this._angle = _maxAngle;
        }
        _handBrake = 0;
    }
}
