﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using Photon.Realtime;
using TMPro;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

namespace MultiSystem
{
    public class PLobby : MonoBehaviourPunCallbacks//lobby controler
    {
        // Start is called before the first frame update
        private static PLobby _pLobbyInstatnce;
        private string _gameVersion = "3";
        [Tooltip("The maximum number of players per room. When a room is full, it can't be joined by new players, and so new room will be created")]
        [SerializeField]
        private byte maxPlayersPerRoom = 4;

        [SerializeField]
        private TextMeshProUGUI _text;
        private TextMeshProUGUI  _connectText;
        [SerializeField]
        private Button[] _buttons;
        [SerializeField]
        private TMP_InputField _nameOfRoom;
        [SerializeField]
        private GameObject _joinRoom, _createRoom,_startMenu;
        private enum ConnectionString
        {
            Connect, Connecting, Connected
        }
        private string _nameOfGameScene = "Room1";


        public static PLobby PLobbyInstatnce
        {
            get
            {
                return _pLobbyInstatnce;
            }
            set
            {
                _pLobbyInstatnce = value;
            }
        }

        void Awake()
        {
            _pLobbyInstatnce = this;
            _buttons[0].onClick.AddListener(Connect);
            _buttons[1].onClick.AddListener(GoOut);
            _buttons[2].onClick.AddListener(SetRoomCreation);
            _connectText = _buttons[0].GetComponentInChildren<TextMeshProUGUI>();
            _createRoom.SetActive(false);
            _joinRoom.SetActive(false);
            Connect();
        }

        // Update is called once per frame

        public void Connect()
        {
            if (!PhotonNetwork.IsConnected)
            {
                _connectText.text = ConnectionString.Connecting.ToString();
                _buttons[0].interactable = false;
                PhotonNetwork.ConnectUsingSettings();
                PhotonNetwork.GameVersion = _gameVersion;
            }
        }
        private void SetRoomCreation()
        {
             PhotonNetwork.CreateRoom(_nameOfRoom.text, new RoomOptions { MaxPlayers = maxPlayersPerRoom ,PublishUserId=true });
        }
        public override void OnConnectedToMaster()
        {
            PhotonNetwork.AutomaticallySyncScene = true;
            PhotonNetwork.JoinLobby();
            _connectText.text = ConnectionString.Connected.ToString();
            _buttons[0].interactable = false;
            _createRoom.SetActive(true);
            _joinRoom.SetActive(true);
        }
        public override void OnDisconnected(DisconnectCause cause)
        {
            _buttons[0].interactable = true;
            _connectText.text = ConnectionString.Connect.ToString();
            _createRoom.SetActive(false);
            _joinRoom.SetActive(false);
        }
        public override void OnJoinRandomFailed(short returnCode, string message)
        {
           // PhotonNetwork.CreateRoom(null, new RoomOptions { MaxPlayers = maxPlayersPerRoom });
        }

        public override void OnJoinedRoom()
        {
            if (PhotonNetwork.CurrentRoom.PlayerCount == 1)
            {

                PhotonNetwork.LoadLevel(_nameOfGameScene);
            }
        }
        public void GoOut()
        {
            PhotonNetwork.Disconnect();
            _startMenu.SetActive(true);
            this.gameObject.SetActive(false);
        }
        private void Update()
        {
            _text.text = PhotonNetwork.GetPing().ToString();
        }
    }
}