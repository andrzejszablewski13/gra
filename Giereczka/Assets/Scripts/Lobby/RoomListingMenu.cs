﻿using Photon.Pun;
using Photon.Realtime;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEditor;
using UnityEngine;

namespace MultiSystem
{
    public class RoomListingMenu : MonoBehaviourPunCallbacks
    {
        private List<RoomListing> _listing = new List<RoomListing>();
        [SerializeField] private RoomListing _roomlisting;
        [SerializeField] private Transform _content;
        [SerializeField] private TextMeshProUGUI _text;
        private int k = 0;

        public override void OnRoomListUpdate(List<RoomInfo> roomList)
        {
            k = 0;
            for (int i = 0; i < roomList.Count; i++)
            {
                if (roomList[i].RemovedFromList)
                {
                    int index = _listing.FindIndex(x => x.RomInfo.Name == roomList[i].Name);
                    if (index != -1)
                    {
                        Destroy(_listing[index].gameObject);
                        _listing.RemoveAt(index);
                    }
                }
                else
                {
                    RoomListing listing = Instantiate(_roomlisting, _content);
                    if (listing != null)
                    {
                        listing.SetRoomInfo(roomList[i]);
                        _listing.Add(listing);
                        listing.gameObject.GetComponent<RectTransform>().localPosition = new Vector3(100, -30 * k, 0);//calculated position of object
                        k++;
                    }
                }
            }
        }
    }
}