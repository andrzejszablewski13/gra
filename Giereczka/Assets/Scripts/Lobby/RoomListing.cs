﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using Photon.Realtime;
using UnityEngine.UI;
using Photon.Pun;

namespace MultiSystem
{
    public class RoomListing : MonoBehaviour//for button of lost to join room
    {
        [SerializeField] private TextMeshProUGUI _text;
        private int _playecCountInRoom, _maxPlayecCountInRoom;
        private string _part1= "/", _part2;
        public RoomInfo RomInfo { get; private set; }
        private bool _setActive = false;
        private Button _button;
        // Start is called before the first frame update
        public void SetRoomInfo(RoomInfo roomInfo)
        {
            RomInfo = roomInfo;
            _button = this.GetComponent<Button>();
            _part2 = ", " + roomInfo.Name;
            _playecCountInRoom = roomInfo.PlayerCount;
            _maxPlayecCountInRoom = roomInfo.MaxPlayers;
            _text.text = _playecCountInRoom + _part1 + _maxPlayecCountInRoom + _part2;
            _button.onClick.AddListener(JoinRoomFromButton);
            _setActive = true;
            
        }
        private void FixedUpdate()
        {
            if(_setActive && _playecCountInRoom != RomInfo.PlayerCount)
            {
                _playecCountInRoom = RomInfo.PlayerCount;
                _text.text = _playecCountInRoom + _part1 + _maxPlayecCountInRoom + _part2;
                if (_playecCountInRoom == _maxPlayecCountInRoom)
                {
                    _button.interactable = false;
                }
                else
                {
                    _button.interactable = true;
                }
            }
        }
        private void JoinRoomFromButton()
        {
            PhotonNetwork.JoinRoom(RomInfo.Name);
        }
    }
}