﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace MultiSystem
{
    public class SelectCar : MonoBehaviour
    {
        // Start is called before the first frame update
        private Slider _slider;
        private string _car = "Car";
        [SerializeField] private Image _green, _orange, _pink, _blue;
        void Awake()
        {
            PlayerPrefs.SetInt(_car, 1);
            _slider = this.GetComponent<Slider>();
        }

        // Update is called once per frame
        public void OnValueChange()
        {
            _green.enabled = false;
            _orange.enabled = false;
            _pink.enabled = false;
            _blue.enabled = false;
            switch (_slider.value)
            {
                case 1:
                    _green.enabled = true;
                    break;
                case 2:
                    _orange.enabled = true;
                    break;
                case 3:
                    _blue.enabled = true;
                    break;
                case 4:
                    _pink.enabled = true;
                    break;
                default:
                    break;
            }
              PlayerPrefs.SetInt(_car, (int)_slider.value);
        }
    }
}