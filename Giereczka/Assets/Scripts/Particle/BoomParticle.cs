﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BoomParticle : MonoBehaviour
{
    [SerializeField] private ParticleSystem _boomParticle;
    private bool _isUsed;

    private void Awake()
    {
        _boomParticle.Stop();
    }

    private void OnEnable()
    {
        DeathOnFall.PlayerRIP += GoBoom;
        KillCollision.DeathDetectedEvent += GoBoom;
    }

    private void OnDisable()
    {
        DeathOnFall.PlayerRIP -= GoBoom;
        KillCollision.DeathDetectedEvent -= GoBoom;
    }

    private void GoBoom()
    {
        if (!_isUsed)
        {
            _boomParticle.Play();
            _isUsed = false;
        }
    }
}
