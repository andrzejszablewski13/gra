﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ParticleSpeedControler : MonoBehaviour//system to control particle giving speed effect-for now a lots of if for debugging and playtest
{
    private Rigidbody _rbTarget;
    private bool _boostActive = false;
    private float _activeTime=-100;
    private ParticleSystem _particle;
    private ParticleSystemRenderer _forSetMaterial;
    [Header("First Car Speed")]
    [Tooltip ("Speed When to start this section")]
    [SerializeField] private float _rBSpeed1 = 0;
    [Tooltip("Set Material On This Speed, if not set then dont change")]
    [SerializeField] private Material _material1;
    [Tooltip("Speed of particle- set -1 to not change")]
    [SerializeField] private float _particleStartSpeed1 = -1;
    [Tooltip("Lifetime of particle- set -1 to not change")]
    [SerializeField] private float _particleLifetime1 = -1;
    [Tooltip("MaxParticles of particle- set -1 to not change")]
    [SerializeField] private int _particleMaxParticles1 = -1;
    [Tooltip("MaxParticles Emission of particle- set  _particleMaxParticles1 to -1 to not change")]
    [SerializeField] private float _particleMaxParticlesEmission1 = -1;
    [Header("Second Car Speed")]
    [Tooltip("Speed When to start this section- set -1 to not change")]
    [SerializeField] private float _rBSpeed2 = -1;
    [Tooltip("Set Material On This Speed, if not set then dont change")]
    [SerializeField] private Material _material2;
    [Tooltip("Speed of particle- set -1 to not change")]
    [SerializeField] private float _particleStartSpeed2 = -1;
    [Tooltip("Lifetime of particle- set -1 to not change")]
    [SerializeField] private float _particleLifetime2 = -1;
    [Tooltip("MaxParticles of particle- set -1 to not change")]
    [SerializeField] private int _particleMaxParticles2 = -1;
    [Tooltip("MaxParticles Emission of particle- set _particleMaxParticles2 to -1 to not change")]
    [SerializeField] private float _particleMaxParticlesEmission2 = -1;
    [Header("Third Car Speed")]
    [Tooltip("Speed When to start this section- set -1 to not change")]
    [SerializeField] private float _rBSpeed3 = -1;
    [Tooltip("Set Material On This Speed, if not set then dont change")]
    [SerializeField] private Material _material3;
    [Tooltip("Speed of particle- set -1 to not change")]
    [SerializeField] private float _particleStartSpeed3 = -1;
    [Tooltip("Lifetime of particle- set -1 to not change")]
    [SerializeField] private float _particleLifetime3 = -1;
    [Tooltip("MaxParticles of particle- set -1 to not change")]
    [SerializeField] private int _particleMaxParticles3 = -1;
    [Tooltip("MaxParticles Emission of particle- set _particleMaxParticles3 to -1 to not change")]
    [SerializeField] private float _particleMaxParticlesEmission3 = -1;
    [Header("OnBoostSpeed one")]
    [Tooltip("Speed When to start this section- set -1 to not change")]
    [SerializeField] private float _rBSpeed4 = -1;
    [Tooltip("Set Material On This Speed, if not set then dont change")]
    [SerializeField] private Material _material4;
    [Tooltip("Speed of particle- set -1 to not change")]
    [SerializeField] private float _particleStartSpeed4 = -1;
    [Tooltip("Lifetime of particle- set -1 to not change")]
    [SerializeField] private float _particleLifetime4 = -1;
    [Tooltip("MaxParticles of particle- set -1 to not change")]
    [SerializeField] private int _particleMaxParticles4 = -1;
    [Tooltip("MaxParticles Emission of particle- set _particleMaxParticles3 to -1 to not change")]
    [SerializeField] private float _particleMaxParticlesEmission4 = -1;
    [Tooltip("Time Of Working")]
    [SerializeField] private float _particleBoostTimeWorking = 4;
    [Header("OnBoostSpeed two")]
    [Tooltip("Speed When to start this section- set -1 to not change")]
    [SerializeField] private float _rBSpeed5 = -1;
    [Tooltip("Set Material On This Speed, if not set then dont change")]
    [SerializeField] private Material _material5;
    [Tooltip("Speed of particle- set -1 to not change")]
    [SerializeField] private float _particleStartSpeed5 = -1;
    [Tooltip("Lifetime of particle- set -1 to not change")]
    [SerializeField] private float _particleLifetime5 = -1;
    [Tooltip("MaxParticles of particle- set -1 to not change")]
    [SerializeField] private int _particleMaxParticles5 = -1;
    [Tooltip("MaxParticles Emission of particle- set _particleMaxParticles3 to -1 to not change")]
    [SerializeField] private float _particleMaxParticlesEmission5 = -1;
    [Tooltip("Time Of Working")]
    [SerializeField] private float _particleBoostTimeWorking2 = 4;
    // Start is called before the first frame update
    void Start()
    {
        _particle = this.GetComponent<ParticleSystem>();
        _forSetMaterial = this.GetComponent<ParticleSystemRenderer>();
        _particle.Stop();
    }
    private void OnEnable()
    {
        PickupButtonManager.SpeedBoostPickupDelegateEvent += Booster;
    }

    private void OnDisable()
    {
        PickupButtonManager.SpeedBoostPickupDelegateEvent -= Booster;
    }
    private void Booster()
    {
        _boostActive = true;
    }
    // Update is called once per frame
    [System.Obsolete]
    void FixedUpdate()
    {
        if(_rbTarget==null)
        {
            try
            {
                _rbTarget = this.gameObject.GetComponentInParent<DriftCamera>()._lookAtTarget.GetComponentInParent<Rigidbody>();
            }
            catch 
            {
                try
                {
                    _rbTarget = this.gameObject.GetComponentInParent<MultiSystem.MultiDriftCamera>()._lookAtTarget.GetComponentInParent<Rigidbody>();
                }
                catch 
                {
                }
                
            }
        }
        else
        {
            if (_boostActive && _rbTarget.velocity.magnitude > _rBSpeed4 && (_rBSpeed5 == -1 || _rbTarget.velocity.magnitude <= _rBSpeed5))
            {

                if (_activeTime == -100)
                {
                    _activeTime = _particleBoostTimeWorking;
                }
                if (!_particle.isPlaying) _particle.Play();
                if (_material5 != null)
                {
                    _forSetMaterial.material = _material4;
                }
                if (_particleStartSpeed4 != -1)
                {
                    _particle.startSpeed = _particleStartSpeed4;
                }
                if (_particleLifetime4 != -1)
                {
                    _particle.startLifetime = _particleLifetime4;
                }
                if (_particleMaxParticles4 != -1)
                {
                    _particle.maxParticles = _particleMaxParticles4;
                }
                if (_particleMaxParticlesEmission4 != -1)
                {
                    _particle.emissionRate = _particleMaxParticlesEmission4;
                }
                _activeTime -= Time.fixedDeltaTime;
                if (_activeTime <= 0)
                {
                    _boostActive = false;
                    _activeTime = -100;
                }
            }
            else if (_boostActive && _rBSpeed5 != -1 && _rbTarget.velocity.magnitude >= _rBSpeed5)
            {
                if (_activeTime == -100)
                {
                    _activeTime = _particleBoostTimeWorking;
                }
                if (!_particle.isPlaying) _particle.Play();
                if (_material5 != null)
                {
                    _forSetMaterial.material = _material5;
                }
                if (_particleStartSpeed5 != -1)
                {
                    _particle.startSpeed = _particleStartSpeed5;
                }
                if (_particleLifetime5 != -1)
                {
                    _particle.startLifetime = _particleLifetime5;
                }
                if (_particleMaxParticles5 != -1)
                {
                    _particle.maxParticles = _particleMaxParticles5;
                }
                if (_particleMaxParticlesEmission5 != -1)
                {
                    _particle.emissionRate = _particleMaxParticlesEmission5;
                }
                _activeTime -= Time.fixedDeltaTime;
                if (_activeTime <= 0)
                {
                    _boostActive = false;
                    _activeTime = -100;
                }
            }
else
            if(_rbTarget.velocity.magnitude < _rBSpeed1)
            {
                _particle.Stop();
            }
            if(_rbTarget.velocity.magnitude>_rBSpeed1 &&(_rBSpeed2==-1 || _rbTarget.velocity.magnitude<= _rBSpeed2))
            {
                if(!_particle.isPlaying)_particle.Play();
                if (_material1 != null)
                {
                    _forSetMaterial.material = _material1;
                }
                if (_particleStartSpeed1 != -1)
                {
                    _particle.startSpeed = _particleStartSpeed1;
                }
                if (_particleLifetime1 != -1)
                {
                    _particle.startLifetime = _particleLifetime1;
                }
                if (_particleMaxParticles1 != -1)
                {
                    _particle.maxParticles = _particleMaxParticles1;
                }
                if(_particleMaxParticlesEmission1!=-1)
                {
                    _particle.emissionRate = _particleMaxParticlesEmission1;
                }
            }else
            if(_rBSpeed2 != -1 && _rbTarget.velocity.magnitude > _rBSpeed2 && (_rBSpeed3 == -1 || _rbTarget.velocity.magnitude <= _rBSpeed3))
            {
                if (!_particle.isPlaying) _particle.Play();
                if (_material2 != null)
                {
                    _forSetMaterial.material = _material2;
                }
                if (_particleStartSpeed2 != -1)
                {
                    _particle.startSpeed = _particleStartSpeed2;
                }
                if (_particleLifetime2 != -1)
                {
                    _particle.startLifetime = _particleLifetime2;
                }
                if (_particleMaxParticles2 != -1)
                {
                    _particle.maxParticles = _particleMaxParticles2;
                }
                if (_particleMaxParticlesEmission2 != -1)
                {
                    _particle.emissionRate = _particleMaxParticlesEmission2;
                }
            }else
            if (_rBSpeed3 != -1 && _rbTarget.velocity.magnitude > _rBSpeed3)
            {
                if (!_particle.isPlaying) _particle.Play();
                if (_material3 != null)
                {
                    _forSetMaterial.material = _material3;
                }
                if (_particleStartSpeed3 != -1)
                {
                    _particle.startSpeed = _particleStartSpeed3;
                }
                if (_particleLifetime3 != -1)
                {
                    _particle.startLifetime = _particleLifetime3;
                }
                if (_particleMaxParticles3 != -1)
                {
                    _particle.maxParticles = _particleMaxParticles3;
                }
                if (_particleMaxParticlesEmission3 != -1)
                {
                    _particle.emissionRate = _particleMaxParticlesEmission3;
                }
            }
        }
    }
}
