﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AIGoesBoom : MonoBehaviour
{
    [SerializeField] private ParticleSystem _boomParticle;
    private bool _isUsed;

    private void Awake()
    {
        _boomParticle.Stop();
    }

    private void OnEnable()
    {
        KillCollision.AIExplosion += GoBoom;
    }

    private void OnDisable()
    {
        KillCollision.AIExplosion += GoBoom;
    }

    private void GoBoom()
    {
        if (!_isUsed)
        {
            _boomParticle.Play();
            _isUsed = false;
        }
    }
}
