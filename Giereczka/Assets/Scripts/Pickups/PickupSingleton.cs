﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PickupSingleton : MonoBehaviour
{
    [SerializeField] private GameObject _pickupGO;
    [SerializeField] private Transform[] _spawnTransform; //tablica gdzie ma się spawnować pickup

    [SerializeField] private GameObject[] _pickupPrefabs = new GameObject[3]; //tablica na prefaby pickupów

    public static int timeToWaitForSpawn = 5; //czas po jakim ma się pojawić nowy pickup na miejscu starego

    private static PickupSingleton _instance;
    public static PickupSingleton Instance
    {
        get
        {
            if (_instance == null)
            {
                Debug.LogError("ERROR: PickupManager not found");
            }
            return _instance;
        }
    }

    private void Awake()
    {
        _instance = this;
        _spawnTransform = new Transform[_pickupGO.transform.childCount];

        for (int i = 0; i < _pickupGO.transform.childCount; i++)
        {
            _spawnTransform[i] = _pickupGO.transform.GetChild(i); 
        }
    }

    private void Start()
    {
        for (int i = 0; i < _spawnTransform.Length; i++)
        {
            Instantiate(_pickupPrefabs[Random.Range(0, 3)], _spawnTransform[i].position, Quaternion.identity);
        }
    }

    public void NewBlock(Vector3 spawnPosition)
    {
        StartCoroutine(SpawnTimer(spawnPosition));
    }

    IEnumerator SpawnTimer(Vector3 whereToSpawn)
    {
        yield return new WaitForSeconds(timeToWaitForSpawn);
        Instantiate(_pickupPrefabs[Random.Range(0, 3)], whereToSpawn, Quaternion.identity); //tutaj się instancjonuje randomowy pickup z tabeli na przyjętej pozycji whereToSpawn
    }
}
