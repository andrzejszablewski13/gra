﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StateDetection : MonoBehaviour //skrypt dać na bazę pickupów
{
    public enum PickupState
    {
        Present,
        WaitingForRespawn
    }

    [SerializeField] private PickupState _currentState;

    public PickupState CurrentState { get => _currentState;}

    private void Awake()
    {
        _currentState = StateDetection.PickupState.Present;
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.GetComponent<Collider>())
        {
            _currentState = StateDetection.PickupState.WaitingForRespawn;
            PickupSingleton.Instance.NewBlock(transform.position);
            StartCoroutine(WaitForRespawn(gameObject));
        }
    }

    IEnumerator WaitForRespawn(GameObject whichObject) //wiem, że nie jest wykorzystywany niby ten parametr, ale bez niego nie działa to indywidualnie dla każdego prefaba na scenie
    { //ta korutyna jest po to, żeby zmienił się state danego pickupa po timeToWaitForSpawn (z klasy PickupSingleton) - czyli wtedy jak pojawi się nowy pickup na miejscu starego
        yield return new WaitForSeconds(PickupSingleton.timeToWaitForSpawn);
        _currentState = StateDetection.PickupState.Present;
    }
}
