﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Jump : MonoBehaviour
{
    public delegate void CollidedJump();
    public static event CollidedJump CollidedJumpEvent;

    private AudioSource _pickUpSound;

    private void Awake()
    {
        _pickUpSound = GetComponent<AudioSource>();
    }

    private void OnTriggerEnter(Collider player)
    {
        _pickUpSound.Play();
        gameObject.GetComponent<Destruction>().ActiveDestruction();
        CollidedJumpEvent();//zmienia funkcję buttona Boost, sygnał do ButtonManager
        Destroy(gameObject);
    }
}
