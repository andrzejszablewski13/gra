﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpeedBoost : MonoBehaviour
{
    public delegate void CollidedSpeedBoost();
    public static event CollidedSpeedBoost CollidedSpeedBoostEvent;

    private AudioSource _pickUpSound;

    private void Awake()
    {
        _pickUpSound = GetComponent<AudioSource>();
    }

    private void OnTriggerEnter(Collider _)
    {

        _pickUpSound.Play();
        gameObject.GetComponent<Destruction>().ActiveDestruction();
        CollidedSpeedBoostEvent(); //zmienia funkcję buttona Boost, sygnał do ButtonManager
        Destroy(gameObject);
    }
}
