﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Eraser : MonoBehaviour
{
    public delegate void CollidedEraser();
    public static event CollidedEraser CollidedEraserEvent;

    private AudioSource _pickUpSound;

    private void Awake()
    {
        _pickUpSound = GetComponent<AudioSource>();
    }

    private void OnTriggerEnter(Collider _)
    {
        _pickUpSound.Play();
        gameObject.GetComponent<Destruction>().ActiveDestruction();
        CollidedEraserEvent(); //zmienia funkcję buttona Boost, sygnał do ButtonManager
        Destroy(gameObject);
    }
}
