﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AIPickupSave : MonoBehaviour
{
    private Collider _jumpColl, _speedBoostCol, _eraserCol;

    public enum PickupType
    {
        None,
        Jump,
        SpeedBoost,
        Eraser
    }

    [SerializeField] private PickupType _pickupSave;

    private void Awake()
    {
        _jumpColl = GetComponent<Jump>().gameObject.GetComponent<Collider>();
        _speedBoostCol = GetComponent<SpeedBoost>().gameObject.GetComponent<Collider>();
        _eraserCol = GetComponent<Eraser>().gameObject.GetComponent<Collider>();
    }

    private void OnTriggerEnter(Collider pickup)
    {
        if (pickup == _jumpColl)
        {
            _pickupSave = AIPickupSave.PickupType.Jump;
        }

        if (pickup == _speedBoostCol)
        {
            _pickupSave = AIPickupSave.PickupType.SpeedBoost;
        }

        if (pickup == _eraserCol)
        {
            _pickupSave = AIPickupSave.PickupType.Eraser;
        }
    }

    private void BoostUsed() //metoda która będzie wywoływana gdy bot zużyje boost
    {
        _pickupSave = AIPickupSave.PickupType.None;
    }
}
