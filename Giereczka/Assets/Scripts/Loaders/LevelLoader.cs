﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LevelLoader : MonoBehaviour
{
    private int _mainLevelScene = 0;

    public void LoadLevelBase()
        {
         SceneManager.LoadScene(_mainLevelScene);
        }
}
