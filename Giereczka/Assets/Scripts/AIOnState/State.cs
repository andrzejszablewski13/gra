﻿using System.Collections.Generic;
using UnityEngine;

//code not fully my but highy modyficated (that means mostly mine)
public class State
{
    // 'States' that the NPC could be in.
    public enum STATE
    {
        IDLE, PURSUE, ATTACK, COLLECT,DODGE, TakePickUp
    };

    // 'Events' - where we are in the running of a STATE.
    public enum EVENT
    {
        ENTER, UPDATE, EXIT
    };

    public STATE name; // To store the name of the STATE.
    protected EVENT stage; // To store the stage the EVENT is in.
    protected GameObject us; // To store the NPC game object.
    protected GameObject target; // To store the transform of the target.
    protected State nextState; // This is NOT the enum above, it's the state that gets to run after the one currently running.
    //rest data for AI
    protected MovementControl controlSystem;
    
    public float _maxDistance = 7.5f;
    protected readonly float _maxPcikUpdistance = 5f;
    protected Vector3 _thisObject, _halfSizeOfCar, _direction;//for detection
    protected bool _barriers = false;
    protected RaycastHit[] _stackOfHits;
    protected List<string> _stackOfNames;
    protected GameObject empty;
    protected readonly float _aboveGround = 0.1f;
    public float _scale = 1.5f;
    protected readonly int _onehondredprocent = 100;
    // Constructor for State
    public State(GameObject _us, GameObject _target, MovementControl _controlSystem,GameObject _empty)
    {
        us = _us;
        empty = _empty;
        stage = EVENT.ENTER;
        target = _target;
        controlSystem = _controlSystem;
        _halfSizeOfCar = us.GetComponentInChildren<Renderer>().bounds.size;
        _halfSizeOfCar = new Vector3(_halfSizeOfCar.x* _scale, _aboveGround, _halfSizeOfCar.z * _scale);
    }

    // Phases as you go through the state.
    public virtual void Enter() { stage = EVENT.UPDATE; } // Runs first whenever you come into a state and sets the stage to whatever is next, so it will know later on in the process where it's going.
    public virtual void Update() { stage = EVENT.UPDATE; } // Once you are in UPDATE, you want to stay in UPDATE until it throws you out.
    public virtual void Exit() { stage = EVENT.EXIT; } // Uses EXIT so it knows what to run and clean up after itself.

    // The method that will get run from outside and progress the state through each of the different stages.
    public State Process()
    {
        if (stage == EVENT.ENTER) Enter();
        if (stage == EVENT.UPDATE) Update();
        if (stage == EVENT.EXIT)
        {
            Exit();
            return nextState; // Notice that this method returns a 'state'.
        }
        return this; // If we're not returning the nextState, then return the same state.
    }
    protected bool CheckifneedToDodge(int rangeofDetection=3)
    {
        CheckForBarriers(us.transform.forward);
        if (IfNeedToDodge()) {
            return true;
        }
            else
         if(target != null&& target!=empty && (target.transform.position - us.transform.position).magnitude > 0)
        {
            if (CheckForBariesrsFromLeftOrRight(true, rangeofDetection))
                return true;
        }
        else
        {
            if (CheckForBariesrsFromLeftOrRight(false, rangeofDetection))
                return true;
        }
        return false;
    }
    protected bool CheckForBariesrsFromLeftOrRight(bool _ifleft, int rangeofDetection)
    {
        int m;
        if (_ifleft)
        {
            m = -1;
        }
        else { m = 1; }
        
            for (int i = 0; i < _onehondredprocent; i++)
            {
                CheckForBarriers(Vector3.Lerp(us.transform.forward, m*us.transform.right, i / _onehondredprocent));
                 if (IfNeedToDodge())
                {
                    return true;
                }
                if(i>= rangeofDetection)
                {
                    return false;
                }
            
        }
        return false;
    }
    protected bool IfNeedToDodge()
    {
        if (_barriers)
        {
            nextState = new Dodge(us, target, controlSystem, empty);
            stage = EVENT.EXIT;
        }
        return _barriers;
    }
    protected void CheckForBarriers(Vector3 _direction)
    {
        int layer = 1 << us.gameObject.layer;
        layer = ~layer;//dont detect himself
        Vector3 _tempus = new Vector3(us.gameObject.transform.position.x, us.gameObject.transform.position.y + _aboveGround, us.gameObject.transform.position.z);//start position
        
        
            _barriers= Physics.BoxCast(_tempus, _halfSizeOfCar, _direction, Quaternion.identity, _maxDistance, layer);//check if is somethink here
            if (_barriers== true)
            {
                _stackOfNames = new List<string>();
                _stackOfHits = Physics.BoxCastAll(_tempus, _halfSizeOfCar, _direction, Quaternion.identity, _maxDistance, layer);//if there is somethink, deteck and add to list
                for (int j = 0; j < _stackOfHits.Length; j++)
                {
                    if (!_stackOfNames.Contains(_stackOfHits[j].collider.name))
                    {
                        _stackOfNames.Add(_stackOfHits[j].collider.name);
                        //for future code update
                    }

                }
            }
        

    }
    protected void CheckForPickUp()//not ended part
    {
        List<GameObject> _listofPickUp;
        _listofPickUp = new List<GameObject>();

       
            for (int i = 0; i < _listofPickUp.Count; i++)
            {
                Vector3 _temporaly = _listofPickUp[i].transform.position - us.transform.position;
                if (_temporaly.magnitude <= _maxPcikUpdistance)
                {
                    
                        break;
                }
            }
       
    }
}
    
    
