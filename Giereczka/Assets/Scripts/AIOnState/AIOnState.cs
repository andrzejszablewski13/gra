﻿using System.Collections;
using UnityEngine;

public class AIOnState : MonoBehaviour
{
    // Variables to handle what we need to send through to our state.
   
    private GameObject _target;
    private static GameObject _empty;
    private MovementControl _controlSystem;
    private IEnumerator _corutin;
    [SerializeField][Range(0.001f,1f)]private float _timeToThink=0.02f;
    [SerializeField] [Range(1f, 25f)] private float _rangeOfDetection = 7.5f;
    [SerializeField] [Range(0.5f, 4f)] private float _scaleOfDetection = 1.5f;
    private State _currentState;

    void Start()
    {
        _corutin = AIThinking();
        _controlSystem = this.gameObject.GetComponent<MovementControl>();
          _currentState = new Idle(CarSingleton.Singleton.ReceiveRestCar(this.gameObject.name), _target, _controlSystem, _empty); // Create our first state.
        if (_empty == null)
        {
            _empty = new GameObject();
            _empty.transform.position = new Vector3(-10, -10, -10);
        }
        StartCoroutine(_corutin);
    }
    IEnumerator AIThinking()
    {
        _currentState._maxDistance = _rangeOfDetection;
        _currentState._scale = _scaleOfDetection;
        _currentState = _currentState.Process(); // Calls Process method to ensure correct state is set.
        yield return new WaitForSeconds(_timeToThink);
    }
    
}
