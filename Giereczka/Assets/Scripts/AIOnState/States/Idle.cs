﻿using UnityEngine;

public class Idle : State
{
    public Idle(GameObject _us, GameObject _target, MovementControl _controlSystem, GameObject _empty)
                : base(_us, _target, _controlSystem, _empty)
    {
        name = STATE.IDLE; // Set name of current state.
    }

    public override void Enter()
    {
        base.Enter(); // Sets stage to UPDATE.
    }
    public override void Update()
    {
        nextState = new Pursue(us, target, controlSystem, empty);
        stage = EVENT.EXIT;
    }

    public override void Exit()
    {
        base.Exit();
    }
}
