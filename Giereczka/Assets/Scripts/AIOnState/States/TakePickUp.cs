﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TakePickUp : State// not ended class
{
    Dictionary<string, GameObject> _temp;
    [SerializeField] private static float _distanceFromTargetToAchieve = 0.5f;
    private GameObject _father;
    
    public TakePickUp(GameObject _us, GameObject _target, MovementControl _controlSystem, GameObject _empty)
                   : base(_us, _target, _controlSystem, _empty)
    {
        name = STATE.PURSUE; // Set name of current state.
    }
    public override void Enter()
    {
        base.Enter(); // Sets stage to UPDATE.
    }
    public override void Update()
    {
        CheckForBarriers(us.transform.forward);
        if (_barriers)
        {
            nextState = new Dodge(us, target, controlSystem,empty);
            stage = EVENT.EXIT;
        }else
        if (target != null && target.activeSelf && target!=empty)
        {
            if (controlSystem.ActualTarget != target.transform.position)
            { controlSystem.ActualTarget = target.transform.position; }

        }
        else
        {
            target = empty;
            nextState = new Idle(us, target, controlSystem,empty);
            stage = EVENT.EXIT;
        }
    }

    public override void Exit()
    {
        base.Exit();
    }
}
