﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pursue : State//pursue other car
{
    Dictionary<string, GameObject> _temp;
    [SerializeField] private static float _distanceFromTargetToAchieve = 4f;
    private GameObject _father;
    public Pursue(GameObject _us, GameObject _target, MovementControl _controlSystem, GameObject _empty)
                   : base(_us, _target, _controlSystem, _empty)
    {
        name = STATE.PURSUE; // Set name of current state.
    }
    public override void Enter()
    {
        if (target == empty)//ffind a target if not set
        {
            List<string> _listofCars;
            _listofCars = CarSingleton.Singleton.GiveListOfCars();
            _father = us.GetComponentInParent<AIOnState>().gameObject;
            int random = new int();//code for random enemy- uncomment to active
            for (int i = 0; i < _listofCars.Count; i++)
            {
                if (_listofCars.Count > 2)
                {
                    random = Random.Range(0, _listofCars.Count - 1);
                }
                if (_listofCars[random] != _father.name)
                {
                    _temp = CarSingleton.Singleton.GiveCar(_listofCars[random]);
                    target = _temp[CarSingleton.Names.LeftCorner.ToString()].gameObject;
                    if (target.activeSelf)
                        break;
                }
            }
            if (target == empty)//if random fail take first free
            {
                for (int i = 0; i < _listofCars.Count; i++)
                {
                    
                    if (_listofCars[i] != _father.name)
                    {
                        _temp = CarSingleton.Singleton.GiveCar(_listofCars[i]);
                        target = _temp[CarSingleton.Names.LeftCorner.ToString()].gameObject;
                        if (target.activeSelf)
                            break;
                    }
                }
            }
            }
        base.Enter(); // Sets stage to UPDATE.
    }
    public override void Update()
    {
        CheckifneedToDodge();
        if (_barriers)
        {
            nextState = new Dodge(us, target, controlSystem,empty);
            stage = EVENT.EXIT;
        }else
        if (target != null && target.activeSelf && target!=empty)//if enemy is set
        {
            if (controlSystem.ActualTarget != target.transform.position)//set  corner as target
            { controlSystem.ActualTarget = target.transform.position; }
            //change corner if we are on one corner, circling
            if(Vector3.Distance(target.transform.position,us.transform.position)<= _distanceFromTargetToAchieve && target!= _temp[CarSingleton.Names.RightCorner.ToString()].gameObject)
            {
                target = _temp[CarSingleton.Names.RightCorner.ToString()].gameObject;
            }else if(Vector3.Distance(target.transform.position, us.transform.position) <= _distanceFromTargetToAchieve && target != _temp[CarSingleton.Names.LeftCorner.ToString()].gameObject)
            {
                target = _temp[CarSingleton.Names.LeftCorner.ToString()].gameObject;
            }
        }
        else
        {
            target = empty;//if enemy is death look for other target
            nextState = new Idle(us, target, controlSystem,empty);
            stage = EVENT.EXIT;
        }
    }

    public override void Exit()
    {
        base.Exit();
    }
}
