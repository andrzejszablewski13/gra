﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Dodge : State//dodge the enemy
{
    [SerializeField] private static float _powerOfTheDodge = 10f;
    private string tempname = "Empty";
    private Vector3 _temp;
    public Dodge(GameObject _us, GameObject _target, MovementControl _controlSystem,GameObject _empty)
                : base(_us, _target, _controlSystem, _empty)
    {
        name = STATE.DODGE; // Set name of current state.
        
    }

    public override void Enter()
    {
        base.Enter(); // Sets stage to UPDATE.
    }
    public override void Update()
    {

        CheckifneedToDodge();

        if (_stackOfHits!=null)//if something before us
        {
           
            for (int i = 0; i < _stackOfHits.Length; i++)//dodge it
            {

                if (_stackOfHits[i].collider.gameObject.layer != us.gameObject.layer)
                {
                    _temp = _stackOfHits[i].point - us.transform.position;
                    if (_temp.magnitude > 0)
                {
                    controlSystem.ActualTarget = us.transform.position + (Vector3.right * _powerOfTheDodge);//to right

                }
                else
                {
                    controlSystem.ActualTarget = us.transform.position + (Vector3.left * _powerOfTheDodge);//or left
                }
                }
            }
            _stackOfHits = null; 
        }
        else
        {
            nextState = new Idle(us, target, controlSystem,empty);//end dodge
            stage = EVENT.EXIT;
        }
      
    }

    public override void Exit()
    {
        base.Exit();
    }
}
