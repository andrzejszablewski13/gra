﻿using System.Collections;
using UnityEngine;

public class MovementControl : MonoBehaviour
{
    private Vector3 _actualTarget;

    private float _verticalAngleForWheel, unityAngle;
    [SerializeField] [Range(0.01f, 1f)] private float _angleGrowPerUpdate=0.02f;
    [SerializeField] [Range(0.001f, 0.1f)] private float _speedOfRecalculation = 0.01f;
    public Vector3 ActualTarget { get => _actualTarget; set => _actualTarget = value; }
    public float VerticalAngleForWheel { get => _verticalAngleForWheel; }
    private Transform _outTransform;
    private Vector3 _usForward, _enemyDirection;
    private IEnumerator _function;
    // Update is called once per frame
    public void AIBegin()
    {
        _function = AiCalculating();
        _outTransform = this.gameObject.transform;
        StartCoroutine(_function);
    }
    private float CalculateAngle()
    {
        _usForward = _outTransform.forward;
        _enemyDirection = ActualTarget- _outTransform.position;
        unityAngle = Vector3.SignedAngle(_usForward, _enemyDirection, this.transform.up);
        return unityAngle;

    }
    private IEnumerator AiCalculating()//main part of AI
    {
        while(true)
            {
            
           _verticalAngleForWheel=Mathf.Lerp(_verticalAngleForWheel, CalculateAngle(), _angleGrowPerUpdate);
            yield return new WaitForSeconds(_speedOfRecalculation);//repet after time
            }
    }
}

