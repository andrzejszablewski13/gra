﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TimeFlow : MonoBehaviour
{
    // Start is called before the first frame update

    [SerializeField] [Range(0.02f, 5f)] private float _timeFlow=1f;
    // Update is called once per frame
    void Update()
    {
        if (Time.timeScale != _timeFlow)
        Time.timeScale = _timeFlow;
    }
}
