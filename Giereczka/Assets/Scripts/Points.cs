﻿using UnityEngine;
using TMPro;

public class Points : MonoBehaviour//points system for future
{
    // Start is called before the first frame update
    private GameObject _temp;
    private TextMeshProUGUI _text;
    private int _pointValue = 1;
    private string _pointsString = "Points";
    void Start()
    {
        if(!PlayerPrefs.HasKey(_pointsString))
        PlayerPrefs.SetInt(_pointsString, 0);
        _temp = Singleton.ReturnText();
        _text = _temp.GetComponent<TextMeshProUGUI>();
        _text.text = PlayerPrefs.GetInt(_pointsString).ToString();
    }

    // Update is called once per frame
    private void OnCollisionEnter(Collision collision)
    {
        if(collision.transform.tag=="Point")
        {
            PlayerPrefs.SetInt(_pointsString, PlayerPrefs.GetInt(_pointsString) +_pointValue);
            PlayerPrefs.Save();
            _text.text = PlayerPrefs.GetInt(_pointsString).ToString();
            Destroy(collision.gameObject);
        }
    }
}
