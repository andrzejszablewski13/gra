﻿using UnityEngine;
using TMPro;

public class CanvasManagement : MonoBehaviour
{
    private float _offsetPosY, _difrence, _angle;
    private Vector3 _offsetPos;
    private Vector2 _screenPoint,_textPosition;
    public RectTransform CanvasRect;

    [SerializeField] private Transform _textFolder;
    private TextMeshProUGUI _text;
    private GameObject _temp;
    private Renderer _parentRend;
    private MeshCollider _sphere;

    private Bounds _bounds;
    private void Awake()
    {
        for (int i = 0; i < CanvasRect.gameObject.transform.childCount; i++)
        {
            if (CanvasRect.gameObject.transform.GetChild(i).name == "Sphere")
            {
                _sphere = CanvasRect.gameObject.transform.GetChild(i).gameObject.GetComponent<MeshCollider>();
            }
            if (CanvasRect.gameObject.transform.GetChild(i).name == "PlaceForUIText")
            {
                _textFolder = CanvasRect.gameObject.transform.GetChild(i);
            }
        }
        _temp = new GameObject();//create text on UI
        _temp.transform.SetParent(_textFolder);
        _temp.AddComponent<TextMeshProUGUI>();
        _text = _temp.GetComponent<TextMeshProUGUI>();
        _text.vertexBufferAutoSizeReduction = true;
        _text.text = "Bot";
        _text.fontSize = 9;
        _temp.GetComponent<RectTransform>().anchorMin = new Vector2(0, 1);
        _temp.GetComponent<RectTransform>().anchorMax = new Vector2(0, 1);
        _temp.GetComponent<RectTransform>().pivot = new Vector2(0, 1);
        _bounds.size = CanvasRect.sizeDelta;
        _parentRend = this.gameObject.GetComponentInParent<Renderer>();
        
        
    }
    public Vector2 AttachUIToGameObject(Vector3 _targetGameObject,float _heightplus)//as name- change position of objedct on scene to its view on canvas
    {
        
        // Offset position above object bbox (in world space)
        _offsetPosY = _targetGameObject.y+ _heightplus;

        // Final position of marker above GO in world space
        _offsetPos = new Vector3(_targetGameObject.x, _offsetPosY, _targetGameObject.z);
        // Calculate screen position (note, not a canvas/recttransform position)
        _screenPoint = Camera.main.WorldToScreenPoint(_offsetPos);

        // Convert screen position to Canvas / RectTransform space <- leave camera null if Screen Space Overlay
        RectTransformUtility.ScreenPointToLocalPointInRectangle(CanvasRect, _screenPoint, null, out Vector2 canvasPos);

        // Set

        return canvasPos;
    }

    private void Update()//our code
    {
        
        _textPosition = this.AttachUIToGameObject(this.gameObject.transform.position, 2f);
        _angle= Vector3.SignedAngle(Camera.allCameras[0].transform.position, this.gameObject.transform.position, this.transform.up);
        if (_parentRend.isVisible)
        {
            _text.GetComponent<RectTransform>().localPosition = _textPosition;//attach text-canvas to objet-scene
            _text.color = Color.black;
            _text.richText = false;
        }
        else
        {
            if (_bounds.Contains(_textPosition))
            {
                _textPosition =new Vector2(0, CanvasRect.sizeDelta.y/2-25);
            }
            else
            {
                _textPosition = _sphere.ClosestPoint(_textPosition);
            }
            
            _text.GetComponent<RectTransform>().localPosition = _textPosition;
            _text.color = Color.blue;
            _text.richText = true;
        }
        _difrence = Vector3.Distance(Camera.allCameras[0].transform.position, this.gameObject.transform.position);//difrence of distance camera-enemy
        _text.GetComponent<RectTransform>().localScale=new Vector3(100/_difrence,100/_difrence,1f);//set size of text depend of distance(above)

    }
    private void OnDisable()
    {
        if(_temp!=null)
        {
            _temp.SetActive(false);
        }
    }
    private void OnEnable()
    {
        if (_temp != null)
        {
            _temp.SetActive(true);
        }
    }
   
}
