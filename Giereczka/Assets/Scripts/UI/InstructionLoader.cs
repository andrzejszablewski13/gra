﻿using UnityEngine;
using UnityEngine.UI;

public class InstructionLoader : MonoBehaviour
{
    [SerializeField] private GameObject _instructionsHandheld;
    [SerializeField] private GameObject _instructionsPC;
    [SerializeField] private GameObject _instructionSkipButton;
    [SerializeField] private Image _backgroundImg;

    private bool _isSystemHandheld;

    private void Awake()
    {
        Time.timeScale = 0;
        if (SystemInfo.deviceType != DeviceType.Handheld)
        {
            _instructionSkipButton.SetActive(false);
            _instructionsHandheld.SetActive(false);
            _instructionsPC.SetActive(true);
            _isSystemHandheld = false;
        }
        else
        {
            _instructionSkipButton.SetActive(true);
            _instructionsHandheld.SetActive(true);
            _instructionsPC.SetActive(false);
            _isSystemHandheld = true;
        }
    }

    private void Update()
    {
        if (!_isSystemHandheld && Input.GetKeyUp(KeyCode.S))//start game
        {
            CloseInstructionAndStartGame();
        }
    }
    public void CloseInstructionAndStartGame()
    {
        if (SystemInfo.deviceType != DeviceType.Handheld)
        {
            _backgroundImg.enabled = false;
            _instructionsPC.SetActive(false);
        }
        else
        {
            _backgroundImg.enabled = false;
            _instructionsHandheld.SetActive(false);
            _instructionSkipButton.SetActive(false);
        }
        Time.timeScale = 1;
    }
}
