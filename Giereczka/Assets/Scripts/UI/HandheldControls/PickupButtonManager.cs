﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class PickupButtonManager : MonoBehaviour
{
    [SerializeField] private Button _boostButton;
    [SerializeField] private TextMeshProUGUI _boostName; //tekst guzika boost

    public delegate void Pickups();
    public static event Pickups SpeedBoostPickupDelegateEvent; //wysyła info do WheelDrive, boost do przyspieszenia (metoda Booster)
    public static event Pickups EraserPickupDelegateEvent; //info do Trail3D, usunięcie traila (met. EraseTrail)
    public static event Pickups JumpPickupDelegateEvent; //info do JumpAndReset

    private AudioSource _onClickSound;

    private void Awake()
    {
        _onClickSound = GetComponent<AudioSource>();
        _boostName.text = "NO BOOST";
    }

    private void OnEnable()
    {
        SpeedBoost.CollidedSpeedBoostEvent += SpeedBoostPickup; //dotyczy SpeedBoost
        WheelDrive.SpeedBoostUsedEvent += BoostUsed; //dotyczy SpeedBoost

        Eraser.CollidedEraserEvent += EraserPickup; //dotyczy Erasera
        Trail3D.EraserUsedEvent += BoostUsed; //dot Erasera

        Jump.CollidedJumpEvent += JumpPickup;
        JumpAndReset.jumpUsed += BoostUsed;
    }

    private void OnDisable()
    {
        SpeedBoost.CollidedSpeedBoostEvent -= SpeedBoostPickup;//dot SpeedBoost
        WheelDrive.SpeedBoostUsedEvent -= BoostUsed; //dot SpeedBoost

        Eraser.CollidedEraserEvent -= EraserPickup; //dotyczy Erasera
        Trail3D.EraserUsedEvent -= BoostUsed; //dot Erasera

        Jump.CollidedJumpEvent -= JumpPickup;
        JumpAndReset.jumpUsed -= BoostUsed;
    }

    private void BoostUsed() //po użyciu boosta usuwa możliwość użycia go jeszcze raz
    {
        _boostName.text = "NO BOOST";
        _boostButton.onClick.RemoveAllListeners();
    }

    private void JumpPickup()
    {
        _boostButton.onClick.RemoveAllListeners();
        _boostName.text = "JUMP!";
        _boostButton.onClick.AddListener(ActiveJump);
    }

    private void ActiveJump()
    {
        _onClickSound.Play();
        JumpPickupDelegateEvent(); //do JumpAndReset
    }

    private void EraserPickup()
    {
        _boostButton.onClick.RemoveAllListeners();
        _boostName.text = "ERASE!";
        _boostButton.onClick.AddListener(ActiveEraser); //nadaje guzikowi możliwość usunięcia traili (Eraser)
    }

    private void ActiveEraser()
    {
        _onClickSound.Play();
        EraserPickupDelegateEvent();
    }

    private void SpeedBoostPickup()
    {
        _boostButton.onClick.RemoveAllListeners();
        _boostName.text = "SPEED BOOST!";
        _boostButton.onClick.AddListener(ActiveSpeedBoost); //nadaje guzikowi możliwość przyspieszenia pojazdu (Speed Boost)
    }

    private void ActiveSpeedBoost()
    {
        _onClickSound.Play();
        SpeedBoostPickupDelegateEvent();
    }
}
