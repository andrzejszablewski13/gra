﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class LRButtons : MonoBehaviour //skrypt odpowiadający za przyciski do skrętów lewo/prawo
{
    public static float _value;
    private Button[] _buttons;
    private EventTrigger[] _eventsTriggers;
    private EventTrigger.Entry[] _entriesEvents;
    private bool _leftClick = false, _richtClick = false;
    [SerializeField] private float _growingrate = 0.02f, _decreseRate = 10f;
    private void Awake()
    {
        _buttons = new Button[2];
        _eventsTriggers = new EventTrigger[2];
        _entriesEvents = new EventTrigger.Entry[4];
        _buttons = GetComponentsInChildren<Button>();
        for (int i = 0; i < _buttons.Length; i++)
        {
            _eventsTriggers[i] = _buttons[i].GetComponent<EventTrigger>();

            switch (i)
            {
                case 0:
                    _entriesEvents[0] = new EventTrigger.Entry
                    {
                        eventID = EventTriggerType.PointerDown
                    };
                    _entriesEvents[0].callback.AddListener((data) => { LeftTurn((PointerEventData)data); });
                    _eventsTriggers[i].triggers.Add(_entriesEvents[0]);
                    _entriesEvents[1] = new EventTrigger.Entry
                    {
                        eventID = EventTriggerType.PointerUp
                    };
                    _entriesEvents[1].callback.AddListener((data) => { LeftTurnDown((PointerEventData)data); });
                    _eventsTriggers[i].triggers.Add(_entriesEvents[1]);

                    break;
                case 1:
                    _entriesEvents[2] = new EventTrigger.Entry
                    {
                        eventID = EventTriggerType.PointerDown
                    };
                    _entriesEvents[2].callback.AddListener((data) => { RightTurn((PointerEventData)data); });
                    _eventsTriggers[i].triggers.Add(_entriesEvents[2]);
                    _entriesEvents[3] = new EventTrigger.Entry
                    {
                        eventID = EventTriggerType.PointerUp
                    };
                    _entriesEvents[3].callback.AddListener((data) => { RightTurnDown((PointerEventData)data); });
                    _eventsTriggers[i].triggers.Add(_entriesEvents[3]);
                    break;
                default:
                    break;
            }

        }
    }
    public void LeftTurn(PointerEventData _)
    {
        _leftClick = true;
    }
    public void LeftTurnDown(PointerEventData _)
    {
        _leftClick = false;
    }

    public void RightTurn(PointerEventData _)
    {
        _richtClick = true;
    }
    public void RightTurnDown(PointerEventData _)
    {
        _richtClick = false;
    }

    private void FixedUpdate()
    {
        if(!_leftClick && !_richtClick)
        {
            _value /= _decreseRate;
        }else if(_leftClick)
        {
            if(_value>-1)
            {
                _value -= _growingrate;
            }
        }else
        {
            if (_value < 1)
            {
                _value += _growingrate;
            }
        }
    }
}
