﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using System.Data.Common;

namespace MultiSystem
{
    public class ShowWhoVote : MonoBehaviour
    {
        // Start is called before the first frame update
        public int _numberOfPlayers;
        [SerializeField] GameObject _placeForText;
        [SerializeField] float _sizeOftext;
        private List<TextMeshProUGUI> _dict = new List<TextMeshProUGUI>();
        public bool _finisched = false;
        private string[] _playersWhoVoted=new string[0];
        private bool _beginned = false;
        private string _nickOfPlayer;
        private void GiveSomeTimesOtherScript()
        {

            _numberOfPlayers = this.gameObject.transform.childCount;
            ShowPlayersNicks();
            _finisched = true;
        }
        // Update is called once per frame
        void Update()
        {
            if(this.transform.childCount>0 &&!_beginned)
            {
                GiveSomeTimesOtherScript();
                Debug.Log(this.transform.childCount);
                _beginned = true;
            }
            if(_finisched && GameManager.InstancePlace.Gamepaused)
            {
                if(_numberOfPlayers!= this.gameObject.transform.childCount)
                {
                    _numberOfPlayers = this.gameObject.transform.childCount;
                    GameObject _temp = new GameObject();
                    _temp.transform.SetParent(_placeForText.transform);
                    _temp.transform.localPosition = Vector3.down * (_numberOfPlayers-1 * _sizeOftext);

                    _nickOfPlayer = this.transform.GetChild(_numberOfPlayers - 1).GetComponent<MovementTransfer>().NickOfPlayer;
                    if (_nickOfPlayer.Equals(""))
                    {
                        _temp.AddComponent<TextMeshProUGUI>().text = "Updating Nick";
                        _temp.name = "Updating Nick";
                    }
                    else
                    {
                        _temp.AddComponent<TextMeshProUGUI>().text = _nickOfPlayer;
                        _temp.name = "NickSet";
                    }
                    _dict.Add(_temp.GetComponent<TextMeshProUGUI>());
                    _dict[_numberOfPlayers-1].fontSize = _sizeOftext;
                    _dict[_numberOfPlayers - 1].color = Color.red;
                    _dict[_numberOfPlayers - 1].alignment = TextAlignmentOptions.CenterGeoAligned;
                }
                for (int i = 0; i < _placeForText.transform.childCount; i++)
                {
                    if(_placeForText.transform.GetChild(i).name == "Updating Nick")
                    {
                        _nickOfPlayer = this.transform.GetChild(i - 1).GetComponent<MovementTransfer>().NickOfPlayer;
                        Debug.Log(this.transform.GetChild(i - 1).GetComponent<MovementTransfer>().NickOfPlayer + " " + this.transform.GetChild(i - 1).name);
                        if (_nickOfPlayer.Equals(""))
                        {
                            _placeForText.transform.GetChild(i).gameObject.GetComponent<TextMeshProUGUI>().text = "Updating Nick";
                            _placeForText.transform.GetChild(i).name = "Updating Nick";
                        }
                        else
                        {
                            _placeForText.transform.GetChild(i).gameObject.GetComponent<TextMeshProUGUI>().text = _nickOfPlayer;
                            _placeForText.transform.GetChild(i).name = "NickSet";
                        }

                    }
                }
                
                if (_playersWhoVoted.Length!=GameManager.InstancePlace._listOfVotedPlayer.Count)
                {
                    for (int i = GameManager.InstancePlace._listOfVotedPlayer.Count - 1; i >= _playersWhoVoted.Length; i--)
                    {
                        _dict[i].color = Color.green;
                    }
                    _playersWhoVoted = new string[GameManager.InstancePlace._listOfVotedPlayer.Count];
                    if(GameManager.InstancePlace._listOfVotedPlayer.Count!=0)
                        for (int i = 0; i < GameManager.InstancePlace._listOfVotedPlayer.Count; i++)
                        {
                            _playersWhoVoted[i] = GameManager.InstancePlace._listOfVotedPlayer[0];
                        }
                }
            }else if(!GameManager.InstancePlace.Gamepaused)
            {
                _placeForText.SetActive(false);
            }
        }
        private void ShowPlayersNicks()
        {
         _numberOfPlayers = this.gameObject.transform.childCount;
            for (int i = 0; i < _numberOfPlayers; i++)
            {
                GameObject _temp = new GameObject();
                _temp.transform.SetParent(_placeForText.transform);
                _temp.transform.localPosition = Vector3.down * (i * _sizeOftext);
                _nickOfPlayer = this.transform.GetChild(_numberOfPlayers - 1).GetComponent<MovementTransfer>().NickOfPlayer;
                if (_nickOfPlayer.Equals(""))
                {
                    _temp.AddComponent<TextMeshProUGUI>().text = "Updating Nick";
                    _temp.name = "Updating Nick";
                }
                else
                {
                    _temp.AddComponent<TextMeshProUGUI>().text = _nickOfPlayer;
                    _temp.name = "NickSet";
                }
                _dict.Add(_temp.GetComponent<TextMeshProUGUI>());
                _dict[i].fontSize = _sizeOftext;
                _dict[i].color = Color.red;
                _dict[i].alignment = TextAlignmentOptions.CenterGeoAligned;

            }
        }
    }
}
