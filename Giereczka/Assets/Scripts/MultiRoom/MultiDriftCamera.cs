﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace MultiSystem
{
    public class MultiDriftCamera : MonoBehaviour//camera for multi
    {
        // Start is called before the first frame update
        [Tooltip("Higher Smothing-camera closer but less smooth camera move")]
        public float _smoothing = 6f;
        [SerializeField] private Transform _cameraPositionWitchoutCar;
        [SerializeField] private float _timebeforeChangeAfterDeath = 2f;
        public Transform _lookAtTarget;//on player car
        public Transform _positionTarget;//starting camera position
        public bool setActive = false;
        private Rigidbody _rb;
        private Vector3 _direction;
        private float _distance,_rbCalibration=1f;
        private float _distanceAntiWarp = 100;

        private void Start()
        {
            if (!this.gameObject.TryGetComponent<Rigidbody>(out _rb))
            {
                _rb = this.gameObject.AddComponent<Rigidbody>();
            }
        }
        private void FixedUpdate()
        {
            if (setActive)
            {
                UpdateCamera();
            }
           
        }
        private void UpdateCamera()
        {
            {
                // transform.position = Vector3.Lerp(transform.position, _positionTarget.position, Time.fixedDeltaTime * _smoothing);//smoth camera follow
                if (_lookAtTarget != null)
                {
                    transform.LookAt(_lookAtTarget);
                    _direction = _positionTarget.position - transform.position;
                    _distance = Vector3.Distance(_positionTarget.position, transform.position);
                    if(_distance> _distanceAntiWarp)
                    {
                        _distance = _distanceAntiWarp;
                    }
                    _rb.velocity = _direction * _distance * _smoothing * _rbCalibration * Time.fixedDeltaTime;
                }else
                {
                    GameManager.InstancePlace.OnDeath();
                    setActive = false;
                    _rb.velocity = Vector3.zero;
                    _rb.useGravity = false;
                    StartCoroutine(OnDeathCameraChange());
                }
            }
        }
        IEnumerator OnDeathCameraChange()
        {
            yield return new WaitForSeconds(_timebeforeChangeAfterDeath);
            transform.position = _cameraPositionWitchoutCar.position;
            transform.eulerAngles = Vector3.right * 90;
        }
    }
}