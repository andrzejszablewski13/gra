﻿using Photon.Pun;
using Photon.Realtime;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace MultiSystem
{
    public class PlayerMenager : MonoBehaviourPunCallbacks//player spawn/destroy
    {
        private PhotonView _photonView;
        private int _spawnerPoint;
        public GameObject _preFabMovementTransfer, _myAvatar;
        public GameObject[] _preFabPlayer;
        public static Dictionary<string, Dictionary<string, GameObject>> _dictionaryOFPlayersObjects;
        // Start is called before the first frame update
        void Awake()
        {
            _photonView = this.gameObject.GetComponent<PhotonView>();
            _dictionaryOFPlayersObjects = new Dictionary<string, Dictionary<string, GameObject>>();//for later destruction on player left


        }
        private void Start()//on player appyring
        {
            if (PhotonNetwork.CurrentRoom.PlayerCount == 1 && PhotonNetwork.IsMasterClient)
            {
                PhotonNetwork.SetMasterClient(PhotonNetwork.LocalPlayer);
                _dictionaryOFPlayersObjects.Add(PhotonNetwork.LocalPlayer.UserId, new Dictionary<string, GameObject>());
            }
            _photonView.RPC(GameManager.StringSave.AddMyCar.ToString(), RpcTarget.MasterClient, PhotonNetwork.LocalPlayer, PlayerPrefs.GetInt(GameManager.StringSave.Car.ToString()));
            GameManager.InstancePlace._playerCount = PhotonNetwork.CurrentRoom.PlayerCount;
        }

        // Update is called once per frame
        public override void OnPlayerEnteredRoom(Player other)
        {
            _dictionaryOFPlayersObjects.Add(other.UserId, new Dictionary<string, GameObject>());
            // _photonView.RPC("AddMyCar", RpcTarget.MasterClient,other, PlayerPrefs.GetInt("Car"));
            GameManager.InstancePlace._playerCount = PhotonNetwork.CurrentRoom.PlayerCount;
        }
        public override void OnPlayerLeftRoom(Player otherPlayer)//destroy on player left
        {
            if (PhotonNetwork.IsMasterClient)
            {
                string id = otherPlayer.UserId;
                PhotonNetwork.Destroy(_dictionaryOFPlayersObjects[id][GameManager.StringSave.Car.ToString()]);
                PhotonNetwork.Destroy(_dictionaryOFPlayersObjects[id][GameManager.StringSave.Control.ToString()]);
                _dictionaryOFPlayersObjects.Remove(id);
                PhotonNetwork.DestroyPlayerObjects(otherPlayer);
            }
        }
        [PunRPC]
        private void AddMyCar(Player player, int carnumber)//Create Car and controler for new player
        {   
            if (PhotonNetwork.IsMasterClient)
            {
                string id = player.UserId;
                _spawnerPoint = PhotonNetwork.CurrentRoom.PlayerCount - 1;

                object[] _dataTemp=new object[1];
                _dataTemp[0] = id;
                GameObject _temp = PhotonNetwork.Instantiate(_preFabPlayer[carnumber-1].name,
                     SceneMenager.Instance._spawnPoints[_spawnerPoint].position,
                     SceneMenager.Instance._spawnPoints[_spawnerPoint].rotation,0, _dataTemp);
                
                GameObject _temp2 = PhotonNetwork.Instantiate(_preFabMovementTransfer.name,Vector3.zero,Quaternion.identity,0, _dataTemp);
                _temp2.GetComponent<PhotonView>().TransferOwnership(_spawnerPoint);

                _temp2.GetComponent<MovementTransfer>()._cameraTarget = _temp.transform;
                _temp.GetComponent<WheelDriveForMultiPlayer>()._movementTransfer = _temp2.transform;
                _temp.GetComponent<WheelDriveForMultiPlayer>().NameOfLayer = GameManager.StringSave.Player.ToString() + player.ActorNumber;

                _dictionaryOFPlayersObjects[id].Add(GameManager.StringSave.Car.ToString(), _temp);
                _dictionaryOFPlayersObjects[id].Add(GameManager.StringSave.Control.ToString(), _temp2);
            }
        }

    }
}