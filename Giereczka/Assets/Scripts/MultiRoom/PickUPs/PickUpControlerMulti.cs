﻿using Photon.Pun;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics.Contracts;
using UnityEngine;

namespace MultiSystem
{
    public class PickUpControlerMulti : MonoBehaviour
    {
        // Start is called before the first frame update
         private GameObject _pickupGO;
         public Transform[] _spawnTransform; //tablica gdzie ma się spawnować pickup

        [SerializeField] private GameObject[] _pickupPrefabs = new GameObject[3]; //tablica na prefaby pickupów

        public static int timeToWaitForSpawn = 5; //czas po jakim ma się pojawić nowy pickup na miejscu starego
        public static PickUpControlerMulti InstancePlace { get; private set; }
        private PhotonView _photonView;
        private int _randomNumber;


        private void Awake()
        {
            _photonView = this.gameObject.GetComponent<PhotonView>();
            _pickupGO = this.gameObject;
            InstancePlace = this;
            _spawnTransform = new Transform[_pickupGO.transform.childCount];

            for (int i = 0; i < _pickupGO.transform.childCount; i++)
            {
                _spawnTransform[i] = _pickupGO.transform.GetChild(i);
            }
        }
        private void Start()
        {
            if(PhotonNetwork.IsMasterClient)
            {
                for (int i = 0; i < _spawnTransform.Length; i++)
                {
                    _randomNumber = Random.Range(0, _pickupPrefabs.Length);
                    _photonView.RPC(GameManager.StringSave.AfterInstantializePickUp.ToString(), RpcTarget.AllBuffered,_randomNumber, i);
                }
            }
        }
        
        public int GetNumberOfSpawnPoint(string name)
        {
            for (int i = 0; i < _spawnTransform.Length; i++)
            {
                if(_spawnTransform[i].name==name)
                {
                    return i;
                }
            }
            return -1;
        }
        public void OnPickUpDestroy(string name)
        {
            StartCoroutine(RespawnNewPickUp(GetNumberOfSpawnPoint(name)));
        }
        IEnumerator RespawnNewPickUp(int i)
        {
            yield return new WaitForSeconds(timeToWaitForSpawn);
            _randomNumber = Random.Range(0, _pickupPrefabs.Length);
            _photonView.RPC(GameManager.StringSave.AfterInstantializePickUp.ToString(), RpcTarget.AllBuffered, _randomNumber,i);
        }
        [PunRPC]
        private void AfterInstantializePickUp(int j, int i)
        {
            GameObject _temp;
            _temp = Instantiate(_pickupPrefabs[j], _spawnTransform[i].position, Quaternion.identity);
            _temp.transform.SetParent(_spawnTransform[i]);
            _temp.SetActive(true);
        }
       
    }
}