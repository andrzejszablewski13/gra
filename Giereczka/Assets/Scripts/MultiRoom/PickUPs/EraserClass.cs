﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace MultiSystem
{
    public class EraserClass : MonoBehaviour
    {
        // Start is called before the first frame update
        public static EraserClass InstanceState { get; private set; }
        private Transform _temp;
        private void Awake()
        {
            InstanceState = this;
        }
        public void UseDeleteTrailOnAll()
        {
            for (int i = 0; i < this.transform.childCount; i++)
            {
                _temp = this.transform.GetChild(i);
                _temp.GetComponentInChildren<Trail3D>().DeleteTrail();
            }
        }
    }
}
