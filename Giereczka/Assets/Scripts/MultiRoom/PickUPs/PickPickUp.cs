﻿using Photon.Pun;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace MultiSystem
{
    public class PickPickUp : MonoBehaviour
    {
        private PhotonView _photonView;
        private string _fatherName;
        private void Awake()
        {

            _photonView= this.gameObject.GetComponent<PhotonView>();
        }
        private void OnTriggerEnter(Collider other)
        {
            if (other.tag == GameManager.StringSave.PickUp.ToString())
            {
                _fatherName = other.gameObject.transform.parent.gameObject.name;
                if (other.name.Contains(GameManager.NamesOfPickUps.Jump.ToString()))
                {
                    this.gameObject.GetComponent<WheelDriveForMultiPlayer>()._movementTransfer.GetComponent<MovementTransfer>().NameOfPickUpActive = GameManager.NamesOfPickUps.Jump.ToString();
                }else
                    if (other.name.Contains(GameManager.NamesOfPickUps.Eraser.ToString()))
                {
                    this.gameObject.GetComponent<WheelDriveForMultiPlayer>()._movementTransfer.GetComponent<MovementTransfer>().NameOfPickUpActive = GameManager.NamesOfPickUps.Eraser.ToString();
                }
                else
                    if (other.name.Contains(GameManager.NamesOfPickUps.Speed.ToString()))
                {
                    this.gameObject.GetComponent<WheelDriveForMultiPlayer>()._movementTransfer.GetComponent<MovementTransfer>().NameOfPickUpActive = GameManager.NamesOfPickUps.Speed.ToString();
                }
                _photonView.RPC(GameManager.StringSave.AfterColectingPickUp.ToString(), RpcTarget.AllBuffered, PickUpControlerMulti.InstancePlace.GetNumberOfSpawnPoint(_fatherName));
            }
        }
       
        [PunRPC]
        private void AfterColectingPickUp(int _father)
        {
            if (_father != -1)
            {
                
                Transform _temp = PickUpControlerMulti.InstancePlace._spawnTransform[_father].GetChild(0);
                _temp.GetComponent<Collider>().enabled = false;
                _temp.GetComponent<AudioSource>().Play();
                _temp.GetComponent<Destruction>().ActiveDestruction();
                _temp.GetComponent<Renderer>().enabled = false;
            }
        }
    }
}