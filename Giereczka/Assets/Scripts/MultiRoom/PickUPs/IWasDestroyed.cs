﻿using MultiSystem;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IWasDestroyed : MonoBehaviour
{
    // Start is called before the first frame update
    private void OnDestroy()
    {
        PickUpControlerMulti.InstancePlace.OnPickUpDestroy(this.transform.parent.name);
    }
}
