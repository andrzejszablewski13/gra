﻿using Photon.Pun;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

namespace MultiSystem
{
    public class MovementTransfer : MonoBehaviour//for transfer players inputs to car
    {
        // Start is called before the first frame update
        public PhotonView _photonView;
        public Transform _cameraTarget;
        public string UserId;
        private bool _cameraset = true;
        private MultiDriftCamera _cameraControl;
        public bool _ready = false;
        private string _nameofPickUpActive;
        [SerializeField] private int _powerOfJump = 50000, _powerOfSpeedUp = 50000;
        private float _valueOfSpeedOnMulti=1f;
        public string NickOfPlayer;
        private bool _posiibleToPickUp = true;
        public string NameOfPickUpActive 
        { 
            get 
            { return _nameofPickUpActive; }
            set
            {
                if (_posiibleToPickUp)
                {
                    if (NameOfPickUpActive != value)
                    {
                        _photonView.RPC(GameManager.StringSave.SendNameofPickUp.ToString(), RpcTarget.AllBuffered, value);
                    }
                    _nameofPickUpActive = value;
                    GameManager.InstancePlace.buttonForPickUp.GetComponentInChildren<TextMeshProUGUI>().text = value;
                    if (_nameofPickUpActive != GameManager.NamesOfPickUps.PickUp.ToString())
                    {
                        GameManager.InstancePlace.buttonForPickUp.interactable = true;
                        _posiibleToPickUp = false;
                        StartCoroutine(PauseForPickingUp());
                    }
                }
                else if (value == GameManager.NamesOfPickUps.PickUp.ToString())
                {
                    _nameofPickUpActive = value;
                    GameManager.InstancePlace.buttonForPickUp.GetComponentInChildren<TextMeshProUGUI>().text = value;
                }
                    
                
            }
                
        }
      


        private  void Awake()
        {
            _photonView = this.GetComponent<PhotonView>();
            UserId = _photonView.InstantiationData[0].ToString();
            this.transform.SetParent(GameManager.InstancePlace.Controlers);
        }
        public void GetAwekenedFully()
        {
            _photonView.RequestOwnership();
                
                _cameraControl = Camera.allCameras[0].transform.GetComponent<MultiDriftCamera>();
                if (!GameManager.InstancePlace.DictionaryOFPlayersObjects.ContainsKey(UserId))
                {
                    GameManager.InstancePlace.DictionaryOFPlayersObjects.Add(UserId, new Dictionary<string, GameObject>());
                    if (!GameManager.InstancePlace.DictionaryOFPlayersObjects[UserId].ContainsKey(GameManager.StringSave.Control.ToString()))
                    {
                        GameManager.InstancePlace.DictionaryOFPlayersObjects[UserId].Add(GameManager.StringSave.Control.ToString(), this.gameObject);
                    }
                }
                else
                {
                    if (!GameManager.InstancePlace.DictionaryOFPlayersObjects[UserId].ContainsKey(GameManager.StringSave.Control.ToString()))
                    {
                        GameManager.InstancePlace.DictionaryOFPlayersObjects[UserId].Add(GameManager.StringSave.Control.ToString(), this.gameObject);
                    }
                }
            GameManager.InstancePlace.buttonForPickUp.interactable = false;
            GameManager.InstancePlace.buttonForPickUp.onClick.AddListener(OnPickClick);
            NickOfPlayer = PhotonNetwork.LocalPlayer.NickName;
            _photonView.RPC(GameManager.StringSave.SendNameofPlayer.ToString(), RpcTarget.All,NickOfPlayer);
            _ready = true;
            
        }
        
        private void Update()
        {            
            
            if (_ready && _photonView.IsMine)
            {
                if (SystemInfo.deviceType != DeviceType.Handheld)//seting control system handheld/PC
                {
                    this.transform.position = new Vector3(Input.GetAxis(GameManager.StringSave.Horizontal.ToString()), 0, Input.GetAxis(GameManager.StringSave.Vertical.ToString()));
                }
                else
                {
                    if ((Input.acceleration.magnitude != 0 && PlayerPrefs.GetInt(SettingsMenu.NameOfPlayerPref.setControlers.ToString())==(int) SettingsMenu.NameForSettingControlers.Default)||
                       PlayerPrefs.GetInt(SettingsMenu.NameOfPlayerPref.setControlers.ToString()) == (int)SettingsMenu.NameForSettingControlers.Accelerator)
                    {
                        this.transform.position = new Vector3(Input.acceleration.x, 0, _valueOfSpeedOnMulti);
                    }
                    this.transform.position = new Vector3(LRButtons._value, 0, _valueOfSpeedOnMulti);
                }
                    if (GameManager.InstancePlace.DictionaryOFPlayersObjects[UserId].ContainsKey(GameManager.StringSave.Car.ToString()) && _cameraset)//connect car witch camera
                {
                    _cameraControl._lookAtTarget = _cameraTarget;
                    _cameraControl._positionTarget = _cameraTarget.GetChild(5).GetChild(1);
                    _cameraControl.setActive = true;
                    _cameraset = false;
                }
            }
        }
        private void OnPickClick()
        {
            if(_nameofPickUpActive== GameManager.NamesOfPickUps.Speed.ToString())
            {
                _photonView.RPC(GameManager.NamesOfPickUps.SpeedUpOnAll.ToString(), RpcTarget.AllBuffered);
            }
            else if(_nameofPickUpActive == GameManager.NamesOfPickUps.Jump.ToString())
            {
                _photonView.RPC(GameManager.NamesOfPickUps.JumpOnAll.ToString(), RpcTarget.AllBuffered);
            }
            else if (_nameofPickUpActive == GameManager.NamesOfPickUps.Eraser.ToString())
            {
                _photonView.RPC(GameManager.NamesOfPickUps.Eraser.ToString(), RpcTarget.AllBuffered);
            }
            GameManager.InstancePlace.buttonForPickUp.interactable = false ;
            NameOfPickUpActive = GameManager.NamesOfPickUps.PickUp.ToString();

        }

        private IEnumerator PauseForPickingUp()
        {
            yield return new WaitForSeconds(1);
            _posiibleToPickUp = true;
        }
        [PunRPC]
        private void Eraser()
        {
            EraserClass.InstanceState.UseDeleteTrailOnAll();
        }
        [PunRPC]
        private void JumpOnAll()
        {
            _cameraTarget.GetComponent<Rigidbody>().AddForce(Vector3.up * _powerOfJump, ForceMode.Impulse);
        }
        [PunRPC]
        private void SpeedUpOnAll()
        {
            _cameraTarget.GetComponent<Rigidbody>().AddForce(Vector3.forward * _powerOfSpeedUp, ForceMode.Impulse);
        }
        private void OnDestroy()
        {
            _photonView.RPC(GameManager.StringSave.DestroyCar.ToString(), RpcTarget.MasterClient);
        }
        [PunRPC]
        private void DestroyCar()
        {
            PhotonNetwork.Destroy(PlayerMenager._dictionaryOFPlayersObjects[UserId][GameManager.StringSave.Car.ToString()]);
        }
        [PunRPC]
        private void SendNameofPickUp(string value)
        {
                _nameofPickUpActive = value;
        }
        [PunRPC]
        private void SendNameofPlayer(string value)
        {
            NickOfPlayer = value;
        }
    }
}