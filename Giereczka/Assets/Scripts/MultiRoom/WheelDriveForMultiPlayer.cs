﻿using Photon.Pun;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace MultiSystem
{
    public class WheelDriveForMultiPlayer : WheelDriveForMulti, IPunObservable//class id dev
    {
        private Rigidbody _rB;
        private PhotonView _photonView;
        public string UserId;
        public Transform _movementTransfer;
        [SerializeField] private bool _simpleSyn, _advSyn, _advSmoothSynOn;
        [SerializeField][Range(0f,2f)] private float _advSmoothSynSlowingPar = 0.2f;
        [SerializeField] private float _forcedTelleportOnSmoothSyn = 10;
        [SerializeField] private GameObject _boomParticle;
        private float[] _synMotorTourqe = new float[4];
        private float _synAngle, _synTempInputLocation;
        private Vector3 _synVelocity, _synPosition, _synRotation;
        private bool _notReadFromOtherClient = true;
        private GameObject _temp;
        private bool _setText = false;
        void Awake()
        {
            _temp = Instantiate(_boomParticle);
            _temp.AddComponent<DestroyOnEnd>()._car = this.gameObject;
            _rB = this.GetComponent<Rigidbody>();
            _photonView = this.GetComponent<PhotonView>();
            UserId = _photonView.InstantiationData[0].ToString();
            if (!GameManager.InstancePlace.DictionaryOFPlayersObjects.ContainsKey(UserId))
            {
                GameManager.InstancePlace.DictionaryOFPlayersObjects.Add(UserId, new Dictionary<string, GameObject>());
                if (!GameManager.InstancePlace.DictionaryOFPlayersObjects[UserId].ContainsKey(GameManager.StringSave.Car.ToString()))
                {
                    GameManager.InstancePlace.DictionaryOFPlayersObjects[UserId].Add(GameManager.StringSave.Car.ToString(), this.gameObject);
                }
            }
            else
            {
                if (!GameManager.InstancePlace.DictionaryOFPlayersObjects[UserId].ContainsKey(GameManager.StringSave.Car.ToString()))
                {
                    GameManager.InstancePlace.DictionaryOFPlayersObjects[UserId].Add(GameManager.StringSave.Car.ToString(), this.gameObject);
                }
            }
            this.transform.SetParent(GameManager.InstancePlace.Cars);

        }
        void Start()
        {
            
                _rb = this.GetComponent<Rigidbody>();
                _mWheels = this.GetComponentsInChildren<WheelCollider>();//find place for wheel

                for (int i = 0; i < _mWheels.Length; ++i)//add wheels
                {
                    WheelCollider wheel = _mWheels[i];

                    // Create wheel shapes only when needed.
                    if (_backWheelShape != null && (_mWheels[i].gameObject.name == "a1l" || _mWheels[i].gameObject.name == "a1r"))
                    {
                        GameObject ws = Instantiate(_backWheelShape);
                        ws.transform.parent = wheel.transform;
                    }
                    else
                    if (_wheelShape != null)
                    {
                        GameObject ws = Instantiate(_wheelShape);
                        ws.transform.parent = wheel.transform;
                    }
                }
                this.gameObject.layer = LayerMask.NameToLayer(NameOfLayer);//set proper layer
                foreach (Transform trans in this.gameObject.GetComponentsInChildren<Transform>(true))//set layer for all children
                {
                    trans.gameObject.layer = LayerMask.NameToLayer(NameOfLayer);
                }
                this.gameObject.GetComponentInChildren<Trail3D>().nameOfLayer = this.NameOfLayer;
                this.gameObject.GetComponentInChildren<Trail3D>()._scaleOfSegment = this._scaleOfSegment;
                //_movementTransfer = GameManager.InstancePlace.DictionaryOFPlayersObjects[UserId][GameManager.StringSave.Control.ToString()].transform;
            
        }
        
        // Update is called once per frame
        void FixedUpdate()
        {
           // if(PhotonNetwork.IsMasterClient)
            this.MovingSystem();
            if(!_setText && _movementTransfer!=null &&_movementTransfer.GetComponent<MovementTransfer>().NickOfPlayer!=null)
            {
                this.gameObject.AddComponent<TextAboveEachPlayer>().CanvasRect = GameManager.InstancePlace.CanvasGameObject.GetComponent<RectTransform>() ;
                _setText = true;
            }
            _temp.transform.position = this.transform.position;
        }
        protected override void MovingSystem()
        {
            _mWheels[0].ConfigureVehicleSubsteps(_criticalSpeed, _stepsBelow, _stepsAbove);
            if (_photonView.IsMine)
            {
                if (SystemInfo.deviceType != DeviceType.Handheld)//seting control system handheld/PC
                {
                    this.ControlsSystem();
                }
                else
                {
                    SpeedUpOnAndroidDevice();
                    RotateByAccelerometer();//on accelorometer-rotate handheld
                }
            }
            else
            {
                //script activate from OnPhotonSerializedView
            }

            MovingSystemCore();
        }
        

        public void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info)
        {
            if(stream.IsWriting)
            {
                _notReadFromOtherClient = true;
               /* for (int i = 0; i < _mWheels.Length; i++)
                {
                    stream.SendNext(this._mWheels[i].motorTorque);
                }*/
                stream.SendNext(this._angle);
                stream.SendNext(this._tempInputLocation);
                stream.SendNext(this._rB.velocity);
                stream.SendNext(this.transform.position);
                stream.SendNext(this.transform.localEulerAngles);
            }
            else
            {
                _notReadFromOtherClient = false;
                /*for (int i = 0; i < _mWheels.Length; i++)
                {
                    _synMotorTourqe[i] = (float)stream.ReceiveNext();
                }*/
                _synAngle= (float)stream.ReceiveNext();
                _synTempInputLocation = (float)stream.ReceiveNext();
                _synVelocity = (Vector3)stream.ReceiveNext();
                _synPosition = (Vector3)stream.ReceiveNext();
                _synRotation = (Vector3)stream.ReceiveNext();
                if (_simpleSyn && !_advSyn)
                {
                    _angle = _synAngle;
                    this._tempInputLocation = _synTempInputLocation;
                    SlowOrSpeedUp();
                    _handBrake = 0;
                }
                else if (_advSyn)
                {
                    _angle = _synAngle;
                    this._tempInputLocation = _synTempInputLocation;
                    _rB.velocity = _synVelocity;
                    if (!_advSmoothSynOn || (transform.position - _synPosition).magnitude > _forcedTelleportOnSmoothSyn)
                    {
                        transform.position = _synPosition;
                        transform.localEulerAngles = _synRotation;
                    }
                    else
                    {
                        if ((transform.forward - _synPosition).magnitude > (transform.position - _synPosition).magnitude)
                        {
                            _rB.velocity -= transform.forward * (_advSmoothSynSlowingPar * (transform.position - _synPosition).magnitude);
                        }
                        else
                        {
                            _rB.velocity += transform.forward * (_advSmoothSynSlowingPar * (transform.position - _synPosition).magnitude);
                        }
                        transform.localEulerAngles = _synRotation;

                    }
                    SlowOrSpeedUp();
                    _handBrake = 0;
                }
            }
        }


        protected override void ControlsSystem()//players Controls on PC
        {
            if (_movementTransfer != null && _notReadFromOtherClient)
            {
                _angle = _maxAngle * _movementTransfer.position.x;
                _tempInputLocation = _movementTransfer.position.z;
            }
            SlowOrSpeedUp();
        }
        public void SpeedUpOnAndroidDevice()//non stop speeding up on handheld
        {
            _tempInputLocation = 1;
        }
        private void RotateByAccelerometer()//rotate by accelerometer on handheld
        {
            if (_movementTransfer != null)
                _angle = _maxAngle * _movementTransfer.position.x;
            SlowOrSpeedUp();
        }
        private void SlowOrSpeedUp()//slowing when not speeding
        {
            if (_tempInputLocation > 0)
            {
                _torque = _maxTorque * _tempInputLocation;
                _handBrake = 0;
            }
            else if (_tempInputLocation == 0)
            {
                _torque = _tempInputLocation;
                _handBrake = _brakeTorque / 10;
            }
            else if (_tempInputLocation < 0)
            {
                _handBrake = _brakeTorque / 2;
            }
        }
        private void OnDestroy()
        {
            _temp = Instantiate(_boomParticle);
            _temp.GetComponent<ParticleSystem>().Stop();
        }
    }
}