﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using Photon.Realtime;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using TMPro;


// Start is called before the first frame update
namespace MultiSystem
{
    public class GameManager : MonoBehaviourPunCallbacks
    {
        public enum StringSave//string save
        {
            Control, Car, Player,
            SendVoteForStart, AddMyCar, StartGame, DestroyCar, SendNameofPickUp, AfterColectingPickUp, AfterInstantializePickUp, SendNameofPlayer,
            Horizontal, OnMyDeath, Vertical, CamRig, CamPosition, PickUp
        }
        public enum NamesOfPickUps
        {
            Speed, Jump, Eraser, PickUp, JumpOnAll, SpeedUpOnAll
        }
        public Dictionary<string, Dictionary<string, GameObject>> DictionaryOFPlayersObjects = new Dictionary<string, Dictionary<string, GameObject>>();//dic for car and control comunication
        public Transform Cars, Controlers;
        public int _playerCount;
        public bool Gamepaused = true;
        public static GameManager InstancePlace { get; private set; }
        private int _voting = 0, _losing = 0;
        private PhotonView _photonView;
        [SerializeField] private Button _buttonForStart;
        [SerializeField] private TextMeshProUGUI _text;
        public Button buttonForPickUp;
        [SerializeField] private GameObject _buttonsHandHeld;
        private string[] _textOnTheEnd = new string[3] { "You Lose", "You Win/Lose against Yourself", "You Win" };
        [HideInInspector] public GameObject CanvasGameObject;
        [HideInInspector] public List<string> _listOfVotedPlayer = new List<string>();

        private void Awake()
        {
            _text.gameObject.SetActive(false);
            _photonView = this.GetComponent<PhotonView>();
            InstancePlace = this;
            PhotonNetwork.MinimalTimeScaleToDispatchInFixedUpdate = 1;
            Time.timeScale = 0;
            _buttonForStart.onClick.AddListener(AddVoteForStart);
            Gamepaused = true;
            if (SystemInfo.deviceType != DeviceType.Handheld)//seting control system handheld/PC
            {
                _buttonsHandHeld.SetActive(false);
            }
            else
            {
                _buttonsHandHeld.SetActive(true);
            }
        }
        private void AddVoteForStart()//vote for game start
        {
            _buttonForStart.interactable = false;
            _photonView.RPC(StringSave.SendVoteForStart.ToString(), RpcTarget.AllBuffered, PhotonNetwork.LocalPlayer.NickName);
        }
        public override void OnLeftRoom()
        {
            PhotonNetwork.Disconnect();
            SceneManager.LoadScene(0);

        }
        public void OnDeath()
        {
            _photonView.RPC(StringSave.OnMyDeath.ToString(), RpcTarget.AllBuffered);
            _text.gameObject.SetActive(true);
            if (_losing < _voting)
            {
                _text.text = _textOnTheEnd[0];
            }
            else if (_losing == _voting && _voting == 1)
            {
                _text.text = _textOnTheEnd[1];
            }
            else
            {
                _text.text = _textOnTheEnd[2];
            }

        }
        [PunRPC]
        private void OnMyDeath()
        {
            _losing++;
        }
        public void LeaveRoom()
        {
            PhotonNetwork.LeaveRoom();
        }
        [PunRPC]
        private void SendVoteForStart(string _nick)
        {
            _listOfVotedPlayer.Add(_nick);
            _voting++;
            if (PhotonNetwork.IsMasterClient && _voting == PhotonNetwork.CurrentRoom.PlayerCount)
            {
                _photonView.RPC(StringSave.StartGame.ToString(), RpcTarget.AllBuffered);
            }
        }
        [PunRPC]
        private void StartGame()//start game
        {
            _buttonForStart.gameObject.SetActive(false);
            Time.timeScale = 1;
            PhotonNetwork.MinimalTimeScaleToDispatchInFixedUpdate = -1;
            Gamepaused = false;
        }
    }
}
