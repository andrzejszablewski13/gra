﻿using Photon.Pun;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace MultiSystem
{
    public class FindMyCar : MonoBehaviour//to locate and connect cotroler witch car of player
    {
        public string _id;
        public int _childrensNumber, _childrensNumber2;
        [SerializeField]private Transform _cars;
        public bool _finded = false;
        public Transform _car, _control;
        [SerializeField] private RectTransform _canvas;
        // Start is called before the first frame update
        void Start()
        {
            _id = PhotonNetwork.LocalPlayer.UserId;
        }

        // Update is called once per frame
        void Update()
        {
            if (!_finded)
            {
                _childrensNumber = this.transform.childCount;
                for (int i = 0; i < _childrensNumber; i++)
                {
                        _control = this.transform.GetChild(i);
                        _childrensNumber2 = _cars.transform.childCount;
                        for (int j = 0; j < _childrensNumber; j++)
                        {
                            if (_cars.transform.GetChild(j).GetComponent<WheelDriveForMultiPlayer>().UserId == this.transform.GetChild(i).GetComponent<MovementTransfer>().UserId)
                            {
                                _car = _cars.transform.GetChild(j);
                                _control.GetComponent<MovementTransfer>()._cameraTarget = _car;
                                _car.GetComponent<WheelDriveForMultiPlayer>()._movementTransfer = _control;
                            if (_cars.transform.GetChild(j).GetComponent<WheelDriveForMultiPlayer>().UserId == _id)
                            {
                                _control.GetComponent<MovementTransfer>().GetAwekenedFully();
                                _finded = true;
                            }

                            }
                        }
                }
            }
                
        }
    }
}
