﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestroyOnEnd : MonoBehaviour
{
    // Start is called before the first frame update
    [SerializeField] private float _timeLife=2;
    [HideInInspector] public GameObject _car;

    void Start()
    {
        this.GetComponent<ParticleSystem>().Stop();
    }
    private void Update()
    {
        if(_car==null)
        {
            this.GetComponent<ParticleSystem>().Play();
            StartCoroutine(DestroyThis());
        }
    }

    // Update is called once per frame
    IEnumerator DestroyThis()
    {
        yield return new WaitForSeconds(_timeLife);
        Destroy(this.gameObject);
    }
}
