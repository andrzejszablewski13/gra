﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace MultiSystem
{
    public class SceneMenager : MonoBehaviour
    {
        // Start is called before the first frame update
        public static SceneMenager Instance { get; private set; }
        public Transform[] _spawnPoints;

        private void OnEnable()
        {
            if (Instance == null)
            {
                Instance = this;
            }
        }

    }
}