﻿using Photon.Pun;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace MultiSystem
{
    public class KillColisionMulti : MonoBehaviour//kill player on multi
    {
        // Start is called before the first frame update
        private PhotonView _photonView;
        private int _layer = 8;
        private Rigidbody _rB;
        private bool _hadRB, _hadPV;
        [SerializeField] private float _sooSlowToKill = 0.2f;
        void Awake()
        {
            _hadPV = this.TryGetComponent<PhotonView>(out _photonView);
            _hadRB = this.TryGetComponent<Rigidbody>(out _rB);
        }
        private void OnCollisionEnter(Collision collision)
        {
            if (collision.collider.gameObject.layer >= _layer)
            {
                if(collision.gameObject.GetComponent<WheelDriveForMultiPlayer>())
                {
                    Elimination(collision.gameObject);
                }
            }
        }
        private void OnCollisionStay(Collision collision)
        {
            if (_hadRB && _rB.velocity.magnitude < _sooSlowToKill)
            {
                if (this.gameObject.GetComponent<WheelDriveForMultiPlayer>())
                {
                    Elimination(collision.gameObject);
                }
            }
        }
        public void Elimination(GameObject _deathTarget)
        {
            if(_hadPV)
            {
                if(_photonView.IsMine)
                 PhotonNetwork.Destroy(this.gameObject);
            }
            else
            {
                _deathTarget.GetComponent<KillColisionMulti>().Elimination(_deathTarget);
            }
        }
    }
}