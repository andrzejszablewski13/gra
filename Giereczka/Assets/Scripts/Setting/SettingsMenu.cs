﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class SettingsMenu : MonoBehaviour
{
    // Start is called before the first frame update
    [SerializeField] private Slider _masterSoundV, _musicV, _soundEffectV;
    [SerializeField] private TMP_Dropdown _setControlers;
    public enum NameOfPlayerPref
    {
        masterVolume,musicVolume,soundEffectVolume,setControlers
    }
    public enum NameForSettingControlers
    {
       Default,Button,Accelerator
    }
    void Awake()
    {
        if (!PlayerPrefs.HasKey(NameOfPlayerPref.masterVolume.ToString()))
        {
            PlayerPrefs.SetFloat(NameOfPlayerPref.masterVolume.ToString(), _masterSoundV.value);
            PlayerPrefs.SetFloat(NameOfPlayerPref.musicVolume.ToString(), _musicV.value);
            PlayerPrefs.SetFloat(NameOfPlayerPref.soundEffectVolume.ToString(), _soundEffectV.value);
            PlayerPrefs.SetInt(NameOfPlayerPref.setControlers.ToString(), _setControlers.value);
        }else
        {
            _masterSoundV.value = PlayerPrefs.GetFloat(NameOfPlayerPref.masterVolume.ToString());
            _musicV.value = PlayerPrefs.GetFloat(NameOfPlayerPref.musicVolume.ToString());
            _soundEffectV.value = PlayerPrefs.GetFloat(NameOfPlayerPref.soundEffectVolume.ToString());
            _setControlers.value = PlayerPrefs.GetInt(NameOfPlayerPref.setControlers.ToString());
        }
        _masterSoundV.onValueChanged.AddListener(SetMasterAudio);
        _musicV.onValueChanged.AddListener(SetMusic);
        _soundEffectV.onValueChanged.AddListener(SetAudioEffect);
        _setControlers.onValueChanged.AddListener(SetControler);
    }

    // Update is called once per frame
    private void SetMasterAudio(float i)
    {
        PlayerPrefs.SetFloat(NameOfPlayerPref.masterVolume.ToString(), i);
    }
    private void SetMusic(float i)
    {
        PlayerPrefs.SetFloat(NameOfPlayerPref.musicVolume.ToString(), i);
    }
    private void SetAudioEffect(float i)
    {
        PlayerPrefs.SetFloat(NameOfPlayerPref.soundEffectVolume.ToString(), i);
    }
    private void SetControler(int i)
    {
        PlayerPrefs.SetInt(NameOfPlayerPref.setControlers.ToString(), i);
    }
}
