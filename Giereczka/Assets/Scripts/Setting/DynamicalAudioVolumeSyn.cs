﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DynamicalAudioVolumeSyn : AudioVolumeSyn
{
    // Start is called before the first frame update
    [SerializeField] private GameObject _synWhenThisActive;

    // Update is called once per frame
    void Update()
    {
        if(_synWhenThisActive.activeSelf)
        {
            base.SetVolume();
        }
    }
}
