﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GoBack : MonoBehaviour
{
    // Start is called before the first frame update
    [SerializeField] private GameObject _mainMenu;
    [SerializeField] private Button _button;
    void Start()
    {
        _button.onClick.AddListener(GoBackToMenu);
    }
    private void GoBackToMenu()
    {
        _mainMenu.SetActive(true);
        this.gameObject.SetActive(false);
    }
    
}
