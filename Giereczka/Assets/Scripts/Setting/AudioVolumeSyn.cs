﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(AudioSource))]
public class AudioVolumeSyn : MonoBehaviour
{
    // Start is called before the first frame update
    [SerializeField] private SettingsMenu.NameOfPlayerPref _typeOfAudio;
    [SerializeField][Range(0,1)] private float _localVolume=1;
    void Start()
    {
        SetVolume();
    }
    protected void SetVolume()
    {
        this.GetComponent<AudioSource>().volume = PlayerPrefs.GetFloat(SettingsMenu.NameOfPlayerPref.masterVolume.ToString()) * _localVolume;
        if (_typeOfAudio == SettingsMenu.NameOfPlayerPref.musicVolume)
        {
            this.GetComponent<AudioSource>().volume *= PlayerPrefs.GetFloat(SettingsMenu.NameOfPlayerPref.musicVolume.ToString());
        }
        else if (_typeOfAudio == SettingsMenu.NameOfPlayerPref.soundEffectVolume)
        {
            this.GetComponent<AudioSource>().volume *= PlayerPrefs.GetFloat(SettingsMenu.NameOfPlayerPref.soundEffectVolume.ToString());
        }
    }

}
