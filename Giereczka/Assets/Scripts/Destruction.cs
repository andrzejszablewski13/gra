﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Destruction : MonoBehaviour//script for destruction animation (its easier for me to wrhite a script than do it in animator)
{
    // Start is called before the first frame update
    private Transform _thisObject ;
    private GameObject _temp;
    public Object[] _stackOfClats2;
    private int _unOfficialNumber = 0;
    public int number=-1;
    [SerializeField] float _time = 0.02f;

    void Start()
    {
        _thisObject= this.gameObject.GetComponent<Transform>();
        
        
    }
    public void ActiveDestruction()
    {
        StartCoroutine("ChangeClat");
    }
    IEnumerator ChangeClat()
    {
        while (true)
        {
            if (this.gameObject.transform.childCount < 2)
            {
                _unOfficialNumber++;
                Destroy(_temp);
                _temp = Instantiate((GameObject)_stackOfClats2[_unOfficialNumber], _thisObject);
            }
            if (_unOfficialNumber >= 30)
            {
                _unOfficialNumber = 0;
                Destroy(_temp);
                Destroy(this.gameObject);
                break;
            }
            yield return new WaitForSeconds(_time);
        }
    }
    // Update is called once per frame
   
}
