﻿using UnityEngine;

public class TrailRemove : MonoBehaviour //skrypt odpowiedzialny za usunięcie traila po eliminacji bota
{
    [SerializeField] private GameObject _segmentForBot;

    private void OnEnable()
    {
        KillCollision.BotElimDetected += RemoveTrail;
    }

    private void OnDisable()
    {
        KillCollision.BotElimDetected -= RemoveTrail;
    }

    private void RemoveTrail()
    {
        _segmentForBot.SetActive(false);
    }
}
