﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DeathOnFall : MonoBehaviour
{
    public delegate void FallDeath();
    public static event FallDeath PlayerRIP;

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.GetComponentInParent<WheelDriveForPlayer>())//if collide with player
        {
            PlayerRIP(); //wysyła info do DeathControl o wyeliminowaniu gracza
        }
    }
}
