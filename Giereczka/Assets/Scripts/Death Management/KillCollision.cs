﻿using UnityEngine;

public class KillCollision : MonoBehaviour //kod kiedy gracz wyeliminuje bota/bot wyeliminuje gracza/bot wyeliminuje bota
{
    public delegate void DeathDetector();
    public static event DeathDetector BotElimDetected;
    public static event DeathDetector DeathDetectedEvent;
    public static event DeathDetector AllBotsElimDetectedEvent;
    public static event DeathDetector AIExplosion;
    private Rigidbody _rB;
    private bool _hadRB;
    [SerializeField] private float _sooSlowToKill = 0.2f;
    private int _layer = 8;

    private void Awake()
    {
        _hadRB = this.TryGetComponent<Rigidbody>(out _rB);
    }
    private void OnCollisionEnter(Collision collision)
    {

        if (collision.gameObject.layer >= _layer)
        {
            if (collision.gameObject.GetComponentInParent<WheelDriveForPlayer>())//if collide with player
            {
                DeathDetectedEvent(); //wysyła info do DeathControl o wyeliminowaniu gracza
            }

            if (collision.gameObject.GetComponentInParent<WheelDriveForBots>())//if collide with bot
            {
                EliminateBot(collision.gameObject);
            }
        }
    }
    private void OnCollisionStay(Collision collision)
    {
        if (_hadRB && _rB.velocity.magnitude < _sooSlowToKill)
        {
            if (this.gameObject.GetComponentInParent<WheelDriveForBots>())
            {
                EliminateBot(collision.gameObject);
            }
            else if (this.gameObject.GetComponentInParent<WheelDriveForPlayer>())
            {
                DeathDetectedEvent();
            }
        }
    }

    private void EliminateBot(GameObject _target) //jeśli bot zostanie wyeliminowany
    {
        AIExplosion();
        PlayerPrefs.SetInt("numberOfAi", PlayerPrefs.GetInt("numberOfAi")-1);//zmniejsza ilość botów o wyeliminowaną jednostkę
        //BotElimDetected(); //wysyła info do TrailRemove żeby usunąć traila wyeliminowanego bota
        if (PlayerPrefs.GetInt("numberOfAi") <= 0) //jeśli ilość botów wynosi zero wysyła informację do DeathControl o wygranej
        {
            AllBotsElimDetectedEvent();
        }
        _target.SetActive(false); //wyłącza pojazd bota
    }
}
