﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class DeathControl : MonoBehaviour //skrypt który uruchamia ekran wygranej/przegranej
{
    private int _winScreenScene = 3;
    private int _deathScreenScene = 2;
    private float _timeBeforeEnd = 1f;


    private void OnEnable()
    {
        DeathOnFall.PlayerRIP += LoadDeathScreen;
        KillCollision.DeathDetectedEvent += LoadDeathScreen;
        KillCollision.AllBotsElimDetectedEvent += LoadEndgameScreen;
    }

    private void OnDisable()
    {
        DeathOnFall.PlayerRIP -= LoadDeathScreen;
        KillCollision.DeathDetectedEvent -= LoadDeathScreen;
        KillCollision.AllBotsElimDetectedEvent -= LoadEndgameScreen;
    }

    private void LoadDeathScreen()
    {
        StartCoroutine(EndGAme(_deathScreenScene));
    }

    private void LoadEndgameScreen()
    {
        StartCoroutine(EndGAme(_winScreenScene));
    }
    IEnumerator EndGAme(int i)
    {
        CarSingleton.Singleton.ClearSingleton();
        yield return new WaitForSeconds(_timeBeforeEnd);
        SceneManager.LoadScene(i);
    }

}
