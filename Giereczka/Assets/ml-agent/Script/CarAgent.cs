﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.MLAgents;
using Unity.MLAgents.Sensors;
using Unity.MLAgents.Sensors.Reflection;
using Unity.MLAgents.Policies;

public class CarAgent : Agent
{
    // Start is called before the first frame update
    [Observable]
    public bool _alive = false;
    [HideInInspector]public ActorCollector _collector;
    
    public bool Alive
    {
        get { return _alive; }
        set
        {
            if ((_immortality && value)|| !_immortality)
            {
                _alive = value;
                if (!_alive)
                {

                    OnDie();
                }
            }
        }
    }
    [SerializeField] private Transform _placeForSegments,_placeForCar;
    [HideInInspector]public int user;
    private Vector3 _localpos;
    private Quaternion _localrot;
    public CarAgent[] targets;
    public string layerNameToForget;
    [SerializeField] private GameObject[] _carsPreFabs;
    [HideInInspector] public GameObject _ourCar;
    [HideInInspector] public Rigidbody _ourCarRB;
    private WheelDriveMLbot _ourDriver;
    private string _curricullum;
    private float _academyNumer;
    private bool trainning;
    private bool _immortality = false;
    private Vector3 _startPos, _difPos;
    private Unity.MLAgents.Policies.BehaviorType _behType;
    private GameObject[] _preSetOfCars;
    public override void Initialize()
    {
/*        Time.timeScale = 1;*/
        _startPos = this.transform.position;
        user = this.gameObject.GetComponent<BehaviorParameters>().TeamId;
        
        _localpos = this.gameObject.transform.localPosition;
        _localrot = this.gameObject.transform.localRotation;
        _preSetOfCars = new GameObject[4];
        for (int i = 0; i < _preSetOfCars.Length; i++)
        {
            _preSetOfCars[i] = Instantiate(_carsPreFabs[i], this.transform.position, this.transform.rotation, _placeForCar);
            _preSetOfCars[i].SetActive(false);
        }
        
        
    }
    private int _selectedCar;
    private void BeginInit()
    {
        _selectedCar = Random.Range(0, _preSetOfCars.Length - 1);
        _ourCar = _preSetOfCars[_selectedCar];
        _ourDriver = _ourCar.GetComponent<WheelDriveMLbot>();
        _ourDriver.agent = this;
        _ourDriver.NameOfLayer = layerNameToForget;
        _ourDriver.placeForSegments = _placeForSegments;
        _ourCar.SetActive(false);
        _ourCar.GetComponent<KillColissionML>().agent = this;
        originalLocalPosition = Vector3.zero;
        originalLocalRotation =new Quaternion(0,0,0,0);
        _ourCarRB = _ourCar.GetComponent<Rigidbody>();
        _difPos = _startPos;
        this.gameObject.GetComponent<RayPerceptionSensorComponent3D>().RayLayerMask =
            this.gameObject.GetComponent<RayPerceptionSensorComponent3D>().RayLayerMask - (int)Mathf.Pow(2, LayerMask.NameToLayer(layerNameToForget));
        _ourCar.gameObject.layer = LayerMask.NameToLayer(layerNameToForget);
        _collector = this.transform.parent.GetComponent<ActorCollector>();
        trainning = _collector.Tranning;
        if (trainning)
        { _curricullum = Academy.Instance.EnvironmentParameters.Keys()[0]; }
        _behType = this.gameObject.GetComponent<BehaviorParameters>().BehaviorType;
        if (_behType == BehaviorType.HeuristicOnly)
        {
            Camera.allCameras[0].transform.GetComponent<DriftCamera>()._lookAtTarget = _ourDriver.transform;
            Camera.allCameras[0].transform.GetComponent<DriftCamera>()._positionTarget = _ourDriver.chiildrens[_ourDriver.chiildrens.Length - 1].transform;
            Camera.allCameras[0].transform.GetComponent<DriftCamera>().setActive = true;
            Camera.allCameras[0].transform.GetComponent<DriftCamera>().enabled = true;
        }
    }
    private Vector3 originalLocalPosition;
    private Quaternion originalLocalRotation;
    public void FollowChild()
    {
       
        transform.position = _ourCar.transform.position;
        this.transform.localEulerAngles =new Vector3(0, _ourCar.transform.localEulerAngles.y,0);



    }
    public override void OnEpisodeBegin()
    {
        BeginInit();
        if (trainning)
        {
            _academyNumer = Academy.Instance.EnvironmentParameters.GetWithDefault(_curricullum, 0);
        }
        else
        {
            _academyNumer = 4;
        }
        if ((!Alive && (_behType==BehaviorType.HeuristicOnly || _behType == BehaviorType.InferenceOnly)) || _behType == BehaviorType.Default)
        {
            this.transform.position = _startPos;
            /*        Debug.Log("test");*/
            this.transform.localRotation = _localrot;
            this.transform.localPosition = _localpos;
            _collector.SetAliveNumber(_academyNumer);
            StartCoroutine(ImortalityOnBegin());
            Alive = true;
/*            for (int i = 0; i < _ourDriver.chiildrens.Length; i++)
            {
                _ourDriver.chiildrens[i].SetActive(true);
            }*/
            _ourCar.SetActive(true);
            _ourCar.transform.localPosition = _localpos;
            _ourCar.transform.localRotation = _localrot;

           
            _ourCar.GetComponent<Collider>().enabled = true;
            _ourCarRB.isKinematic = false;
        }
    }
    private int transformUnitmultiplayer = 7;
    private IEnumerator ImortalityOnBegin()
    {
        _immortality = true;
        yield return new WaitForSeconds(1f);
        _immortality = false;
    }
    private float angle;
    public override void CollectObservations(VectorSensor sensor)
    {
        FollowChild();
        sensor.AddObservation(Alive);
        sensor.AddOneHotObservation(_selectedCar, 4);
    }
    public override void OnActionReceived(float[] vectorAction)
    {
        if(_alive)
        {
            FollowChild();
            /*            AddReward(-0.01f);*/
            angle = vectorAction[0];
            _ourDriver.angle = angle;
            if(_ourCar.transform.localRotation.x >= 90 || _ourCar.transform.localRotation.z >= 90 || _ourCar.transform.position.y<-100)
                {
                    Alive = false;
/*                    Debug.Log("test4 "+ Mathf.Abs(_ourCar.transform.eulerAngles.x)+" "+ Mathf.Abs(_ourCar.transform.eulerAngles.z));
*/                } 
        }

    }
    public void Death()
    {
        _alive = false;
        _ourCar.SetActive(false);
/*        for (int i = 0; i < _ourDriver.chiildrens.Length; i++)
        {
            _ourDriver.chiildrens[i].SetActive(false);
        }*/
        _ourCar.GetComponent<Collider>().enabled = false;
        _ourCarRB.velocity = Vector3.zero;
        _ourCarRB.isKinematic = true;
        _ourCar.SetActive(false);
        _alive = false;
        AddReward(-0.2f);
    }
    private void OnDie()
    {
        /*Debug.Log("test3");*/
        
        Death();
        AddReward(-0.1f);
        _collector.alivenumber -= 1;
    }

    public override void Heuristic(float[] actionsOut)
    {
        actionsOut[0] = Input.GetAxis("Horizontal");
    }

}
