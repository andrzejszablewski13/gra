﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ActorCollector : MonoBehaviour
{
    private CarAgent[] _agents;
    private int _alivenumber=99;
    private float _academyNumber;
    [SerializeField] private GameObject _segBorders,_startSpawnPoint,_pointPreFab,_pointStack,_activePointStack;
    public bool Tranning = false;
    
    public int alivenumber
    {
        get { return _alivenumber; }
        set
        {
            _alivenumber = value;
/*             Debug.Log(_alivenumber);*/
            if(_alivenumber<=1 && _academyNumber>=1)
            {
                EndEpisode();
            }
            if(_alivenumber <= 0 && _academyNumber < 1)
            {
                EndTrainingEpisode();
            }
        }
    }
    private int _numberOfPoins = 20;
    private void Awake()
    {
        _corutin = SpawnPointCorutin();
        for (int i = 0; i < _numberOfPoins; i++)
        {
            GameObject _temp = Instantiate(_pointPreFab,Vector3.zero,Quaternion.identity, _pointStack.transform);
            MovePoint _temp2 = _temp.GetComponent<MovePoint>();
            _temp2.points = _pointStack;
            _temp2.activepoints = _activePointStack;
            /*_temp.transform.SetParent(_pointStack.transform);*/
            _temp.SetActive(false);
        }
        _agents = this.transform.GetComponentsInChildren<CarAgent>();
        if(Tranning)
        {
            _academyNumber = 0;
            _segBorders.SetActive(true);
        }
        else
        {
            _academyNumber = 99;
            _segBorders.SetActive(false);
        }
    }
    private bool _setted = false;
    public void SetAliveNumber(float number)
    {
        if (!_setted)
        {
            _setted = true;
            alivenumber = _agents.Length;
            _academyNumber = number;

            switch (_academyNumber)
            {
                case 0:
                    _segBorders.SetActive(false);
                    SpawnStarterFreePoints();
                    break;
                case 1:
                    _segBorders.SetActive(false);
                    StartCoroutine(_corutin);
                    break;
                case 2:
                    _segBorders.SetActive(false);
                    StartCoroutine(_corutin);
                    break;
                default:
                    _segBorders.SetActive(false);
                    StartCoroutine(_corutin);
                    break;
            }
        }
    }
    public void PointCollected()
    {
        if(_pointStack.transform.childCount>=20 && _academyNumber==0)
        {
            EndTrainingEpisode();
        }
    }
    private float[] _speedvalue = new float[5] { 2, 3, 5, 10, 15 };
    private float[] _sizeValue = new float[5] { 25, 10, 5, 2, 1 };
    private void SpawnStarterFreePoints()
    {
        for (int i = 0; i < _startSpawnPoint.transform.childCount; i++)
        {
            GameObject _temp = _pointStack.transform.GetChild(0).gameObject;
            _temp.GetComponent<MovePoint>().speedValue = _speedvalue[(int)_academyNumber];
            _temp.GetComponent<MovePoint>().sizeMulti = _sizeValue[(int)_academyNumber];
            _temp.GetComponent<MovePoint>().BeforeEnebled();
           _temp.transform.localPosition = _startSpawnPoint.transform.GetChild(i).localPosition;
            _temp.transform.localRotation = _startSpawnPoint.transform.GetChild(i).localRotation;
            _temp.SetActive(true);
        }
    }
    private IEnumerator _corutin;
    IEnumerator SpawnPointCorutin()
    {
        yield return new WaitForSeconds(0.01f);
        if (_pointStack.transform.childCount > 2)
        {
            GameObject _temp = _pointStack.transform.GetChild(0).gameObject;
            _temp.GetComponent<MovePoint>().speedValue = _speedvalue[(int)_academyNumber];
            _temp.GetComponent<MovePoint>().sizeMulti = _sizeValue[(int)_academyNumber];
            _temp.GetComponent<MovePoint>().BeforeEnebled();
            _temp.transform.localPosition = new Vector3(Random.Range(-250, 250), 0.6f, Random.Range(-250, 250));
            _temp.transform.localEulerAngles = new Vector3(0, Random.Range(-180, 180), 0);
            _temp.SetActive(true);
        }
    }
    private void DeactivatePoibts()
    {
        StopCoroutine(_corutin);
        for (int i = _activePointStack.transform.childCount - 1; i >= 0; i--)
        {
            GameObject _temp = _activePointStack.transform.GetChild(i).gameObject;
            _temp.GetComponent<MovePoint>().BeforeDisable();
            _temp.SetActive(false);
        }
    }
    private void EndEpisode()
    {
        _setted = false;
        _alivenumber = 99;
/*        Debug.Log("test2");*/
        for (int i = 0; i < _agents.Length; i++)
        {
            if (_agents[i].Alive)
            {
                _agents[i].AddReward(0.5f);
                _agents[i].Death();

            }
            
        }
        for (int i = 0; i < _agents.Length; i++)
        {
            _agents[i].EndEpisode();
        }
        DeactivatePoibts();
    }
    private void EndTrainingEpisode()
        
    {
        _setted = false;
        DeactivatePoibts();
        for (int i = 0; i < _agents.Length; i++)
        {
            if (_agents[i].Alive)
            {
                _agents[i].AddReward(0.2f);
                _agents[i].Death();

            }
        }
        for (int i = 0; i < _agents.Length; i++)
        {
            _agents[i].EndEpisode();
        }
    }
}
