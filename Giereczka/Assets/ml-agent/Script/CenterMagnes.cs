﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CenterMagnes : MonoBehaviour
{
    // Start is called before the first frame update
    private void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.TryGetComponent<WheelDriveMLbot>(out WheelDriveMLbot t))
        {
            t.agent.AddReward(0.002f);
        }
    }
}
