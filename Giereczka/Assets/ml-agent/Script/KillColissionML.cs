﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KillColissionML : MonoBehaviour
{
    // Start is called before the first frame update
    public CarAgent agent;

    protected virtual void OnCollisionEnter(Collision collision)
    {
        if(collision.collider.gameObject.tag == ("Point"))
        {

            collision.collider.gameObject.GetComponent<MovePoint>().BeforeDisable();
            collision.collider.gameObject.SetActive(false);
       /*     if(agent!=null)
            {
                agent.AddReward(0.4f);
                agent._collector.PointCollected();
            }*/
        }
        if(collision.collider.gameObject.tag==("Player") )
        {
            
            if (this.gameObject.tag != ("Player"))
            {

                /* Debug.Log(this.gameObject.name);*/
                collision.collider.gameObject.GetComponent<WheelDriveMLbot>().agent.Alive = false;

                if (agent != null && collision.collider.gameObject.GetComponent<WheelDriveMLbot>().agent != agent)
                {
                    agent.AddReward(0.7f);
                }
                else
                {
                    collision.collider.gameObject.GetComponent<WheelDriveMLbot>().agent.AddReward(-0.2f);
                }
            }
            else if (this.gameObject.tag == ("Player") && collision.collider.gameObject.GetComponent<WheelDriveMLbot>().agent.Alive == true)
            {

                collision.collider.gameObject.GetComponent<WheelDriveMLbot>().agent.AddReward(-0.4f);
                collision.collider.gameObject.GetComponent<WheelDriveMLbot>().agent.Death();
                this.gameObject.GetComponent<WheelDriveMLbot>().agent.AddReward(-0.4f);
                this.gameObject.GetComponent<WheelDriveMLbot>().agent.Death();
                this.gameObject.GetComponent<WheelDriveMLbot>().agent._collector.alivenumber -= 2;
            }
        }
    }
}
