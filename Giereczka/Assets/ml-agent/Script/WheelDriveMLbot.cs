﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WheelDriveMLbot : wheeldrive
{
    // WheelDrive modyficated for bots
    public float angle = 0;
    public CarAgent agent;
    public Transform placeForSegments;
    private bool _setValue=false;
    public GameObject[] chiildrens;
    void Start()
    {
        base.BeforeScriptStart();
    }

    // Update is called once per frame
    void Update()
    {
        if(!_setValue && agent!=null && placeForSegments!=null)
        {
            _trail.agent = agent;
            _trail._segmentLocation= placeForSegments;
            _trail.OnactivateFirst();
            _setValue = true;
        }
        _mWheels[0].ConfigureVehicleSubsteps(_criticalSpeed, _stepsBelow, _stepsAbove);
        this.MovingSystem();
    }
    protected override void MovingSystem()
    {
        ControlsSystem();
        base.MovingSystem();
    }
    protected override void ControlsSystem()//geting rotation from AI
    {
        this._torque = this._maxTorque;
        //this._angle = controlSystem.VerticalAngleForWheel;
       
            this._angle = angle*this._maxAngle;
        _handBrake = 0;
    }
}
