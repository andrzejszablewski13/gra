﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovePoint : MonoBehaviour
{
    // Start is called before the first frame update
    public bool canRotate = false;
    public float speedValue = 1,sizeMulti=1;
    private Rigidbody _rb;
    [HideInInspector] public GameObject points,activepoints;
    void Start()
    {
        _rb = this.gameObject.GetComponent<Rigidbody>();
    }
    private void OnDisable()
    {
        
    }
    public void BeforeDisable()
    {
        if (points != null)
            this.transform.SetParent(points.transform);
    }
    public void BeforeEnebled()
    {
        this.gameObject.GetComponent<BoxCollider>().size = new Vector3(sizeMulti, 4, sizeMulti);

        if (activepoints != null)
            this.transform.SetParent(activepoints.transform);
    }
    private void OnEnable()
    {
       
    }
    // Update is called once per frame
    void Update()
    {
        _rb.velocity = Vector3.forward * speedValue;

    }
}
